package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils

import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.Movement.isRunEnabled
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.Movement.runEnergy
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.Movement.toggleRun
import kotlin.random.Random.Default.nextInt

class RunEnergyManager {
    private var runActivation: Int
    fun trigger() {
        if (isRunEnabled) return
        if (runEnergy >= runActivation) {
            toggleRun(true)
            sleep(1000, 2000)
            runActivation = randomRunTarget
        }
    }

    private val randomRunTarget: Int
        private get() = nextInt(1, 30)

    init {
        runActivation = randomRunTarget
    }
}