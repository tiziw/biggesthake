package net.runelite.client.plugins.biggesthake

import com.google.inject.Binder
import com.google.inject.Provides
import net.runelite.api.Client
import net.runelite.api.NPC
import net.runelite.api.events.AnimationChanged
import net.runelite.api.events.GameStateChanged
import net.runelite.api.events.InteractingChanged
import net.runelite.client.Notifier
import net.runelite.client.callback.ClientThread
import net.runelite.client.config.ConfigManager
import net.runelite.client.eventbus.EventBus
import net.runelite.client.eventbus.Subscribe
import net.runelite.client.game.ItemManager
import net.runelite.client.plugins.Plugin
import net.runelite.client.plugins.PluginDescriptor
import net.runelite.client.plugins.biggesthake.api.rs.misc.getLogger
import net.runelite.client.plugins.biggesthake.core.BHHandler
import net.runelite.client.plugins.biggesthake.core.EventManager
import net.runelite.client.plugins.biggesthake.core.ScriptManager
import net.runelite.client.plugins.biggesthake.util.RandomHandler
import net.runelite.client.ui.ClientUI
import net.runelite.client.ui.overlay.OverlayManager
import org.slf4j.LoggerFactory
import javax.inject.Inject

@PluginDescriptor(name = "Big Hake Plugin", description = "Show helpful information", tags = ["overlay"])
class BHPlugin : Plugin() {
    private val logger = LoggerFactory.getLogger(BHPlugin::class.java)
    @Inject
    lateinit var client: Client
    @Inject
    lateinit var clientUI: ClientUI
    @Inject
    lateinit var config: BHConfiguration
    @Inject
    lateinit var overlay: BHOverlay
    @Inject
    lateinit var overlayManager: OverlayManager
    @Inject
    lateinit var itemManagr: ItemManager
    @Inject
    lateinit var clientThread: ClientThread
    @Inject
    lateinit var notifier: Notifier
    @Inject
    lateinit var eventBus: EventBus

    private lateinit var handler: BHHandler

    @Throws(Exception::class)
    override fun startUp() {
        eventBus.register(EventManager)
        handler = BHHandler(client, clientUI, config, clientThread, notifier)
        val map = client.collisionMaps
        logger.info("Example plugin started!")
        overlayManager.add(overlay)
    }

    @Throws(Exception::class)
    override fun shutDown() {
        logger.info("Example plugin stopped!")
        handler.shutDown()
        overlayManager.remove(overlay)
    }

    override fun configure(binder: Binder) {
        binder.bind(BHOverlay::class.java)
    }

    @Provides
    internal fun provideConfig(configManager: ConfigManager): BHConfiguration {
        return configManager.getConfig(BHConfiguration::class.java)
    }

}

