package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.localPlayer
import net.runelite.client.plugins.biggesthake.api.rs.misc.getLogger
import java.lang.Math.abs
import java.util.*
import kotlin.random.Random.Default.nextInt

/**
 * Builds a cache of local area for pathfinding.
 *
 * ONLY works with tiles loaded within region data. i.e: `Scene.getFCollisionFlags()`
 * - Always 104 x 104
 */
class BFSMapCache constructor(val start: WorldPoint = localPlayer.worldLocation, region: Region = Region()) : BFS(region) {
    private lateinit var parentMap: Array<Array<Tile?>?>
    private lateinit var costMap: Array<IntArray>
    var accessibleArea = 0
        private set

    override fun getPath(start: Tile?, end: Tile?): List<Tile?> {
        val path: MutableList<Tile?> = ArrayList()
        var temp = end
        while (temp != null) {
            path.add(temp)
            temp = getParent(temp)
        }
        path.reverse()
        return path
    }

    fun canReach(position: WorldPoint): Boolean {
        if (!region.contains(position)) return false
        if (position == localPlayer.worldLocation) return true
        val tile = getTile(position)
        return tile != null && getCost(tile) != Int.MAX_VALUE
    }

    fun getMoveCost(position: WorldPoint): Int {
        if (!region.contains(position)) return Int.MAX_VALUE
        if (position == localPlayer.worldLocation) return 0
        val tile = getTile(position)
        return tile?.let { getCost(it) } ?: Int.MAX_VALUE
    }

    /**
     *
     * Collision aware random selector
     *
     * @param maxDistance
     * @return
     */
    fun getRandom(maxDistance: Int): WorldPoint { // TODO: write an actual method for this...
        val startX = start.x - base.x
        val startY = start.y - base.y
        for (i in 0..49999) {
            val x = nextInt(-maxDistance, maxDistance)
            val y = nextInt(-maxDistance, maxDistance)
            if (costMap[startX + x][startY + y] < maxDistance) return WorldPoint(startX + x, startY + y, localPlayer.worldLocation.plane)
        }
        return start
    }

    private fun getCost(tile: Tile): Int {
        return costMap[tile.x - base.x][tile.y - base.y]
    }

    fun getParent(position: WorldPoint): WorldPoint? {
        if (!region.contains(position)) return null
        val tile = getTile(position) ?: return null
        return if (getParent(tile) != null) getParent(tile).toWorldPoint() else null
    }

    private fun getParent(tile: Tile): Tile {
        return parentMap[tile.x - base.x]!![tile.y - base.y]!!
    }

    private fun calculate(start: WorldPoint) {
        parentMap = Array<Array<Tile?>?>(map.size) { arrayOfNulls(map.size) }
        costMap = Array(map.size) { IntArray(map.size) }
        // Initialize CostMap.
        for (i in costMap.indices) for (j in costMap[i].indices) costMap[i][j] = Int.MAX_VALUE
        val initial = getTile(start)
        if (initial == null || initial.isBlocked) return
        val queue = LinkedList<Tile>()
        queue.push(initial)
        costMap[initial.x - base.x][initial.y - base.y] = 0
        getLogger().info("Calculating ${queue.size}")
        while (!queue.isEmpty()) {
            val tile = queue.pop()
            accessibleArea++
            for (neighbor in getNeighbors(tile)) {
                if (costMap[neighbor.x - base.x][neighbor.y - base.y] != Int.MAX_VALUE) continue
                costMap[neighbor.x - base.x][neighbor.y - base.y] = costMap[tile.x - base.x][tile.y - base.y] + 1

                parentMap[neighbor.x - base.x]!![neighbor.y - base.y] = tile
                queue.addLast(neighbor)
            }
        }
    }

    fun isBlocked(worldPoint: WorldPoint, distance: Int): Boolean {
        val startX = abs(worldPoint.x - base.x)
        val startY = abs(worldPoint.y - base.y)
        if(costMap.size <= startX || costMap[startX].size <= startY) return true
        if (costMap[startX][startY] < distance) return false

        return false
    }

    init {
        calculate(start)
    }
}