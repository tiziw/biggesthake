package net.runelite.client.plugins.biggesthake.util.font;

public class FontHolder {

    // TEST
//    public static void main(String[] args) {
//        String c = "a";
//        int width = FontUtil.getFontWidth(c.codePointAt(0));
//        int cpoint = c.codePointAt(0);
//        FontUtil.FontInfo f = FontUtil.getFonts()[cpoint - 32];
//        for(int i = 0;i < FontUtil.getFonts().length;i++) {
//            int charcode = 32 + i;
//            assert(FontUtil.getFonts()[32+i].getCodePoint() == charcode);
//        }
//
//        System.out.println(cpoint == f.getCodePoint() ? "" : "false");
//    }

    public static final String fonts =
            "[" + "\n  {\n" +
            "    \"codePoint\" : 32,\n" +
            "    \"height\" : 0,\n" +
            "    \"width\" : 0\n" +
            "  }, {\n" +
            "    \"codePoint\" : 33,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 34,\n" +
            "    \"height\" : 4,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 35,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 12\n" +
            "  }, {\n" +
            "    \"codePoint\" : 36,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 37,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 10\n" +
            "  }, {\n" +
            "    \"codePoint\" : 38,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 11\n" +
            "  }, {\n" +
            "    \"codePoint\" : 39,\n" +
            "    \"height\" : 4,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 40,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 41,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 42,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 43,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 44,\n" +
            "    \"height\" : 4,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 45,\n" +
            "    \"height\" : 1,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 46,\n" +
            "    \"height\" : 1,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 47,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 48,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 49,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 50,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 51,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 52,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 53,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 54,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 55,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 56,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 57,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 58,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 59,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 60,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 61,\n" +
            "    \"height\" : 4,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 62,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 63,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 64,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 12\n" +
            "  }, {\n" +
            "    \"codePoint\" : 65,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 66,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 67,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 68,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 69,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 70,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 71,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 72,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 73,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 74,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 75,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 76,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 77,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 78,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 79,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 80,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 81,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 82,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 83,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 84,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 85,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 86,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 87,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 88,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 89,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 90,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 91,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 92,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 93,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 94,\n" +
            "    \"height\" : 5,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 95,\n" +
            "    \"height\" : 1,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 96,\n" +
            "    \"height\" : 4,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 97,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 98,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 99,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 100,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 101,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 102,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 103,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 104,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 105,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 106,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 107,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 108,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 109,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 110,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 111,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 112,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 113,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 114,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 115,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 116,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 117,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 118,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 119,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 120,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 121,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 122,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 123,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 124,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 125,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 126,\n" +
            "    \"height\" : 3,\n" +
            "    \"width\" : 10\n" +
            "  }, {\n" +
            "    \"codePoint\" : 8364,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 161,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 162,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 163,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 9\n" +
            "  }, {\n" +
            "    \"codePoint\" : 164,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 165,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 166,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 167,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 168,\n" +
            "    \"height\" : 2,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 169,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 10\n" +
            "  }, {\n" +
            "    \"codePoint\" : 170,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 171,\n" +
            "    \"height\" : 5,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 172,\n" +
            "    \"height\" : 3,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 173,\n" +
            "    \"height\" : 1,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 174,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 10\n" +
            "  }, {\n" +
            "    \"codePoint\" : 175,\n" +
            "    \"height\" : 1,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 176,\n" +
            "    \"height\" : 4,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 177,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 178,\n" +
            "    \"height\" : 5,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 179,\n" +
            "    \"height\" : 5,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 180,\n" +
            "    \"height\" : 3,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 181,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 182,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 183,\n" +
            "    \"height\" : 2,\n" +
            "    \"width\" : 2\n" +
            "  }, {\n" +
            "    \"codePoint\" : 184,\n" +
            "    \"height\" : 3,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 185,\n" +
            "    \"height\" : 5,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 186,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 187,\n" +
            "    \"height\" : 5,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 188,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 9\n" +
            "  }, {\n" +
            "    \"codePoint\" : 189,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 9\n" +
            "  }, {\n" +
            "    \"codePoint\" : 190,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 9\n" +
            "  }, {\n" +
            "    \"codePoint\" : 191,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 192,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 193,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 194,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 195,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 196,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 197,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 198,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 12\n" +
            "  }, {\n" +
            "    \"codePoint\" : 199,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 200,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 201,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 202,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 203,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 204,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 205,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 206,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 207,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 208,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 209,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 210,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 211,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 212,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 213,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 214,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 215,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 216,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 10\n" +
            "  }, {\n" +
            "    \"codePoint\" : 217,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 218,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 219,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 220,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 221,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 222,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 223,\n" +
            "    \"height\" : 10,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 224,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 225,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 226,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 227,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 228,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 229,\n" +
            "    \"height\" : 11,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 230,\n" +
            "    \"height\" : 6,\n" +
            "    \"width\" : 11\n" +
            "  }, {\n" +
            "    \"codePoint\" : 231,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 232,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 233,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 234,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 235,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 236,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 237,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 3\n" +
            "  }, {\n" +
            "    \"codePoint\" : 238,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 5\n" +
            "  }, {\n" +
            "    \"codePoint\" : 239,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 4\n" +
            "  }, {\n" +
            "    \"codePoint\" : 240,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 241,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 242,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 243,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 244,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 245,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 246,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 247,\n" +
            "    \"height\" : 7,\n" +
            "    \"width\" : 8\n" +
            "  }, {\n" +
            "    \"codePoint\" : 248,\n" +
            "    \"height\" : 8,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 249,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 250,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 251,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 252,\n" +
            "    \"height\" : 9,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 253,\n" +
            "    \"height\" : 13,\n" +
            "    \"width\" : 6\n" +
            "  }, {\n" +
            "    \"codePoint\" : 254,\n" +
            "    \"height\" : 12,\n" +
            "    \"width\" : 7\n" +
            "  }, {\n" +
            "    \"codePoint\" : 255,\n" +
            "    \"height\" : 13,\n" +
            "    \"width\" : 6\n" +
            "  }" + "\n]";
            //"}\n";
}
