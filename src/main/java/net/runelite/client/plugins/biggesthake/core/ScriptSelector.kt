package net.runelite.client.plugins.biggesthake.core

import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JTextField

class ScriptSelector(handler: BHHandler) : JFrame() {
    private var scriptRunning = false

    init {
        title = "Biggest Hake"
        setSize(390, 300)
        setLocation(100, 150)
        //make sure it quits when x is clicked
        defaultCloseOperation = EXIT_ON_CLOSE
        //set look and feel
        setDefaultLookAndFeelDecorated(true)
        val labelM = JLabel("Biggest Hake Current script: " + ScriptManager.scriptEnum.name)
        labelM.setBounds(50, 50, 200, 30)
        val textField = JTextField("test")
        //textField.text = "${WorldPoint.fromLocal(UIHelper.client, UIHelper.client.localDestinationLocation).toString()}"
        textField.setBounds(100, 200, 200, 30)

        val button = JButton("Start script")
        button.addActionListener {
            if (scriptRunning) {
                ScriptManager.stop()
                button.text = "Start script"
                scriptRunning = false
            } else {
                ScriptManager.start()
                button.text = "Stop script"
                scriptRunning = true
            }
        }
        //set size of the text box
        button.setBounds(50, 100, 200, 30)
        //add elements to the frame
        add(textField)
        add(labelM)
        add(button)

        defaultCloseOperation = DISPOSE_ON_CLOSE
        layout = null
        isVisible = true
    }

    fun shutDown() {
        isVisible = false
        dispose()
    }
}
