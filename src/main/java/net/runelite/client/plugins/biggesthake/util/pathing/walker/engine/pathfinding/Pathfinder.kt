package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

import net.runelite.api.coords.WorldPoint
import java.util.*

abstract class Pathfinder(val region: Region) {
    val base: WorldPoint = region.base
    val map: Array<Array<Tile?>> = region.map
    abstract fun getPath(start: Tile?, end: Tile?): List<Tile?>
    fun getPath(start: WorldPoint, end: WorldPoint): List<WorldPoint>? {
        val a = getTile(start)
        if (a == null || a.isBlocked) return null
        val b = getTile(end)
        return if (b == null || b.isBlocked) null else convert(getPath(a, b))
    }

    fun getNeighbors(tile: Tile): List<Tile> {
        val neighbors: MutableList<Tile> = ArrayList()
        for (direction in Direction.values()) {
            if (!isValidDirection(direction, tile)) continue
            val neighbor = getTile(direction, tile) ?: continue
            neighbors.add(neighbor)
        }
        return neighbors
    }

    fun getTile(position: WorldPoint): Tile? {
        if (!region.contains(position)) return null
        if (!isValidTile(position.x, position.y)) return null
        val x = position.x - base!!.x
        val y = position.y - base.y
        return map!![x]!![y]
    }

    fun convert(path: List<Tile?>): List<WorldPoint> {
        val positions: MutableList<WorldPoint> = ArrayList()
        for (tile in path.filterNotNull()) {
            positions.add(WorldPoint(tile.x, tile.y, tile.z))
        }
        return positions
    }

    private fun isValidDirection(direction: Direction, tile: Tile?): Boolean {
        if (tile == null) return false
        val destination = getTile(direction, tile)
        if (destination == null || destination.isBlocked) return false
        if (!direction.isDiagonal) {
            return when (direction) {
                Direction.NORTH -> !tile.isBlockedN
                Direction.EAST -> !tile.isBlockedE
                Direction.SOUTH -> !tile.isBlockedS
                Direction.WEST -> !tile.isBlockedW
                else -> throw IllegalStateException("There can only be 4 non diagonal neighbors...")
            }
        }
        when (direction) {
            Direction.NORTH_EAST -> return (isValidDirection(Direction.NORTH, tile) && isValidDirection(Direction.EAST, getTile(Direction.NORTH, tile))
                    && isValidDirection(Direction.EAST, tile) && isValidDirection(Direction.NORTH, getTile(Direction.EAST, tile)))
            Direction.SOUTH_WEST -> return (isValidDirection(Direction.SOUTH, tile) && isValidDirection(Direction.WEST, getTile(Direction.SOUTH, tile))
                    && isValidDirection(Direction.WEST, tile) && isValidDirection(Direction.SOUTH, getTile(Direction.WEST, tile)))
            Direction.SOUTH_EAST -> return (isValidDirection(Direction.SOUTH, tile) && isValidDirection(Direction.EAST, getTile(Direction.SOUTH, tile))
                    && isValidDirection(Direction.EAST, tile) && isValidDirection(Direction.SOUTH, getTile(Direction.EAST, tile)))
            Direction.NORTH_WEST -> return (isValidDirection(Direction.NORTH, tile) && isValidDirection(Direction.WEST, getTile(Direction.NORTH, tile))
                    && isValidDirection(Direction.WEST, tile) && isValidDirection(Direction.NORTH, getTile(Direction.WEST, tile)))
        }
        throw IllegalStateException("No handler for direction: $direction")
    }

    private fun getTile(direction: Direction, tile: Tile): Tile? {
        val x = direction.translateX(tile.x) - base!!.x
        val y = direction.translateY(tile.y) - base.y
        return if (isValidTile(x, y)) map!![x]!![y] else null
    }

    private fun isValidTile(x: Int, y: Int): Boolean {
        var x = x
        var y = y
        if (x > 104 || y > 104) {
            x = x - base!!.x
            y = y - base.y
        }
        return x >= 0 && y >= 0 && x < map!!.size && y < map.size
    }
}