package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.Constants
import net.runelite.api.GameObject
import net.runelite.api.NPC
import net.runelite.api.widgets.Widget
import net.runelite.api.widgets.WidgetInfo
import net.runelite.api.widgets.WidgetItem
import net.runelite.client.plugins.biggesthake.api.rs.misc.*
import net.runelite.client.plugins.biggesthake.enums.EntityType
import net.runelite.client.plugins.biggesthake.enums.InteractOption
import net.runelite.client.plugins.biggesthake.enums.InteractOption.NONE
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.input.getPoint
import net.runelite.client.plugins.biggesthake.input.getRawPoint
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import java.awt.Rectangle

const val MAX_RETRIES = 3
val screenCanvas = Rectangle(0, 0, Constants.GAME_FIXED_WIDTH, Constants.GAME_FIXED_HEIGHT)

fun interactor(entityType: EntityType, rectFun: () -> Rectangle?, name: String = "", targetName: String? = "", leftClick: Boolean = false): Boolean {
    try {
        var clickPoint = rectFun()?.getPoint(entityType.randomConsumer)
        if (clickPoint == null || !screenCanvas.contains(clickPoint)
                || (entityType.distanceDependant && !client.getWidget(WidgetInfo.FIXED_VIEWPORT).contains(clickPoint.convert()))) {
            if (!entityType.distanceDependant) {
                throw Exception("Unable to find ${entityType.name}")
            }
            // Rotate until target is visible
            rotateCamera {
                val rec = rectFun()
                if(rec != null) {
                    val point = rec.getPoint()
                    point != null
                            && entityType.distanceDependant && !client.getWidget(WidgetInfo.FIXED_VIEWPORT).contains(point.convert())
                }
                false
            }

            clickPoint = rectFun()?.getPoint(entityType.randomConsumer)
            clickPoint ?: throw Exception("Unable to find ${entityType.name}")
        }
        // Check if target is visible
        clickPoint = rectFun()?.getPoint(entityType.randomConsumer)
        clickPoint ?: throw Exception("Can't locate click target")

        // Display click area (debug)
        rectFun()?.let { addClickRectangle(it, entityType.randomConsumer) }

        // Move mouse on target position so menu entries appear
        Input.mouseMove(clickPoint.x, clickPoint.y)
        // Wait until the menuEntries appear (human reaction delay included)
        sleepUntil({ client.menuEntries.isNotEmpty() }, 2000, 10)

        if (leftClick) {
            Input.leftClick()
            return true
        }

        // Loop through menu entries to find the right one
        // Menu entries are based on what the mouse is pointing at
        // Context Menu does not have to be open yet
        var index = -1
        for (i in client.menuEntries.indices) {
            val entry = client.menuEntries[i]
            if (entry.option == name && entry.targetName() == targetName) {
                index = i
            }
        }

        // We want the index, since the bounds function needs it
        if (index != -1) {
            val lastIndex = client.menuEntries.size - 1
            if (index == lastIndex) { // Left click, option we want is the first one
                Input.leftClick()
            } else {
                Input.rightClick()
                sleepUntil({ client.isMenuOpen }, 3000)
                sleepRand()

                val bounds = client.getMenuEntryBounds(index)

                // Needs to be raw point, coords are already
                // stretched because calculation uses canvas mouse pos
                val point = bounds.getRawPoint { x, y -> getMenuClickPoint(x, y) }

                addClickRectangle(bounds) { w, h -> getMenuClickPoint(w, h) }
                Input.mouseMove(point!!.x, point!!.y)

                Input.leftClick()
            }
            return true
        } else {
            // TODO: moving target, right click misses
            // TODO: Retries!
            throw Exception("Could not find right menu entry $name with target $targetName and entries ${client.menuEntries.map { it.option }}")
        }
    } catch (e: Exception) {
        e.printStackTrace();
        return false
    }
}

fun doInteract(entityType: EntityType, bounds: () -> Rectangle?,
               action: String = "", targetName: String? = "", leftClick: Boolean = false, timeoutTask: (() -> Boolean)? = null): Boolean {
    var count = 0
    while (count++ <= MAX_RETRIES) {
        val result = interactor(entityType, bounds, action, targetName, leftClick)
        if (timeoutTask != null)
            sleepUntil(timeoutTask, 3000, onTimeout = {})

        if ((timeoutTask != null && timeoutTask()) || result)
            return true
        else
            sleep(1000, 2300)
    }
    return false
}

fun Widget.interact(option: InteractOption = NONE, action: String = option.asString, isFinished: (() -> Boolean)? = option.timeoutTask): Boolean {
    return doInteract(EntityType.WIDGET, { this.bounds }, action, this.name(), timeoutTask = isFinished)
}

fun Widget.click(): Boolean {
    return doInteract(EntityType.WIDGET, { this.bounds }, leftClick = true)
}

fun WidgetItem.interact(option: InteractOption = NONE, action: String = option.asString, isFinished: (() -> Boolean)? = option.timeoutTask): Boolean {
    return doInteract(EntityType.WIDGET, { this.canvasBounds }, action, client.getItemDefinition(this.id).name, timeoutTask = isFinished)
}

fun NPC.interact(option: InteractOption = NONE, action: String = option.asString): Boolean {
    // TODO: distance check, if 2 far walk 2 him, or zoom out?
    return doInteract(EntityType.NPC, { this.convexHull.bounds }, action, this.name)
}

fun GameObject.interact(option: InteractOption = NONE, action: String = option.asString): Boolean {
    return doInteract(EntityType.GAMEOBJECT, { this.convexHull.bounds }, action, this.getComposition().name)
}

fun GameObject.hasAction(condition: (String) -> Boolean): Boolean {
    return this.actions().firstOrNull { condition(it) } != null
}

fun NPC.hasAction(condition: (String) -> Boolean): Boolean {
    return this.composition.actions.filterNotNull().find(condition) != null
}

fun GameObject.interact(regex: (String) -> Boolean): Boolean {
    val action = this.actions().find { regex.invoke(it) }
    action ?: return false//throw Exception("Unable to find action $action on gameobject ${this.getName()}")

    return doInteract(EntityType.GAMEOBJECT, { this.convexHull.bounds }, this.getName(), action)
}


