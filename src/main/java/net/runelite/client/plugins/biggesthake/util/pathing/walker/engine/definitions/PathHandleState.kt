package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions

enum class PathHandleState {
    SUCCESS, FAILED, EXIT
}