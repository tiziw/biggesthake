package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

class BulkBankPathRequest(val player: PlayerDetails, val requests: List<BankPathRequestPair>)