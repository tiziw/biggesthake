package net.runelite.client.plugins.biggesthake.input

import net.runelite.client.plugins.biggesthake.input.GuiHelper.Companion.mouseX
import net.runelite.client.plugins.biggesthake.input.GuiHelper.Companion.mouseY
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.canvas
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.frame
import java.awt.AWTKeyStroke
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent
import java.awt.event.MouseEvent.*
import java.awt.event.MouseWheelEvent
import java.awt.event.MouseWheelEvent.WHEEL_UNIT_SCROLL


open class EventRobot() {

    open fun mousePress(buttonID: Int) {
       //UIHelper.processMouseEvent(MouseEvent(canvas, MOUSE_PRESSED, System.currentTimeMillis(), 0, mouseX, mouseY, 1, false, InputEvent.BUTTON1_MASK buttonID))
    }

    open fun mouseMove(x: Int, y: Int) {
        GuiHelper.processMouseEvent(MouseEvent(canvas, MOUSE_MOVED, System.currentTimeMillis(), 0, x,  y, 0, false, 0))
    }

    open fun mouseRelease(buttonID: Int) {
        GuiHelper.processMouseEvent(MouseEvent(canvas, MOUSE_RELEASED, System.currentTimeMillis(), 0, mouseX, mouseY, 1, false, buttonID))
        GuiHelper.processMouseEvent(MouseEvent(canvas, MOUSE_CLICKED, System.currentTimeMillis(), 0, mouseX, mouseY, 1, false, buttonID))
    }

    open fun mouseWheel(down: Boolean) {
        GuiHelper.processMouseEvent(MouseWheelEvent(canvas, MOUSE_WHEEL, System.currentTimeMillis(), 0, mouseX, mouseY, 1, false, WHEEL_UNIT_SCROLL, 3, if (down) 1 else -1 )) // To change body of created functions use File | Settings | File Templates.
    }

    open fun keyRelease(keyCode: Int) {
        GuiHelper.processKeyEvent(KeyEvent(frame, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, keyCode, keyCode.toChar(), KeyEvent.KEY_LOCATION_STANDARD))
    }

    open fun enterKey(number: Int) {
        enterKey(Character.forDigit(number, 10))
    }

    open fun enterKey(key: Char) {
        val keyStroke = AWTKeyStroke.getAWTKeyStroke(key)
        GuiHelper.processKeyEvent(KeyEvent(canvas, KeyEvent.KEY_TYPED, System.currentTimeMillis(), keyStroke.modifiers, keyStroke.keyCode, keyStroke.keyChar))
    }

    open fun keyPress(keyCode: Int) {
        //UIHelper.processKeyEvent(KeyEvent(frame, KeyEvent.KEY_TYPED, System.currentTimeMillis(), 0, 0, keyCode.toChar(), KeyEvent.KEY_LOCATION_UNKNOWN))
        GuiHelper.processKeyEvent(KeyEvent(canvas, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, keyCode.toChar(), KeyEvent.KEY_LOCATION_STANDARD))
    }
}