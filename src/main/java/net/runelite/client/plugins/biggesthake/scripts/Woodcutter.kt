package net.runelite.client.plugins.biggesthake.scripts

import net.runelite.api.ObjectID
import net.runelite.api.events.AnimationChanged
import net.runelite.client.plugins.biggesthake.LocalScripts
import net.runelite.client.plugins.biggesthake.api.rs.*
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.api.script.BaseScript
import net.runelite.client.plugins.biggesthake.api.script.Event
import net.runelite.client.plugins.biggesthake.enums.InteractOption
import net.runelite.client.plugins.biggesthake.util.pathing.Area
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.RSBank


class Woodcutter : BaseScript(LocalScripts.WC) {

    init {
        setParams(
                area = Area(WorldPoint(3202, 3506), WorldPoint(3225, 3498)),
                autoBank = true,
                preferredBank = RSBank.GRAND_EXCHANGE,
                keepItems = arrayOf("Rune axe")
        )
    }


    @Event(onStart = true, type = AnimationChanged::class)
    fun onAnimationChanged(): Boolean {
        if(!localPlayer.isAnimating()) {
            sleep(2000, 5000)
            val yewTree = getGameObject(ObjectID.YEW)
            if (yewTree != null) {
                return yewTree.interact(InteractOption.CHOP_DOWN)
                //sleepUntil({ Inventory.contains(ItemID.YEW_LOGS) })
            } else {
                logger.warn("Tree not found")
            }
        }
        return false
    }
}