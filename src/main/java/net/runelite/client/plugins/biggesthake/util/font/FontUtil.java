package net.runelite.client.plugins.biggesthake.util.font;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;

import java.util.concurrent.atomic.AtomicInteger;

public class FontUtil {
     public static FontInfo[] fonts = new Gson().fromJson(FontHolder.fonts, FontInfo[].class);

        @AllArgsConstructor
        class FontInfo {
                public int codePoint;
                public int height;
                public int width;
        }

        static int getFontWidth(int code) {
            int index = code - 32;
            return fonts[index].width;
        }

        public static int stringWidth(String string) {
            final AtomicInteger totalWidth = new AtomicInteger(0);
            string.codePoints().forEach(c -> {
                totalWidth.addAndGet( getFontWidth(c) + 2);
            });
            return totalWidth.get();
        }

        public static int getFontHeight(int code) {
            int index = code - 32;
            return fonts[index].height;
        }
}