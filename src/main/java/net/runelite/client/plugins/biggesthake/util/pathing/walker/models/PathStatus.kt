package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

enum class PathStatus {
    UNMAPPED_REGION, SUCCESS, BLOCKED_END, BLOCKED_START, EXCEEDED_SEARCH_LIMIT, UNREACHABLE, NO_WEB_PATH
}