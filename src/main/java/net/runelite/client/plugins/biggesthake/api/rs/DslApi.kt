package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.GameObject


fun getObject(name: String, function: (GameObject) -> Boolean): Boolean {
    for(i in 0..2) {
        val gameObject = getNearestObjects().find { it.getName() == name }
        // TODO: distance, rotation? interact prolly not best place 2 do it
        gameObject?:continue

        if(function(gameObject)) {
            return true
        }
    }

    return false
}