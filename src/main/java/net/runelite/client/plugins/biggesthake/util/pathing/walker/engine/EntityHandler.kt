package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine

import net.runelite.api.NPC
import net.runelite.api.coords.WorldPoint
import net.runelite.api.widgets.Widget
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.api.rs.*
import net.runelite.client.plugins.biggesthake.enums.EntityType

import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathHandleState
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.StrongHoldAnswers
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding.BFSMapCache
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.CharterShip
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.RunManager
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.regex.Pattern

object EntityHandler {
    private val LIKELY = Pattern.compile("(?i)(yes|ok(ay)?|sure|alright|fine|(.*search away.*)|(.*can i.*?)).*")
    private val UNLIKELY = Pattern.compile("(?i)no(.|.thank.*|.sorry*+)?")
    @JvmStatic
    fun handleWithAction(action: Pattern, start: WorldPoint?, end: WorldPoint, walkCondition: WalkCondition): PathHandleState {
        val target = getNearestNPCs().stream()
                .filter { npc: NPC -> npc.hasAction { s: String? -> action.matcher(s).matches() } }.findFirst().orElse(null)
                ?: return PathHandleState.FAILED
        val option = Arrays.stream(target.composition.actions).filter { s: String? -> action.matcher(s).matches() }.findFirst().get()
        if (!interactor(EntityType.NPC, { target.convexHull.bounds }, option, target.name, false)) return PathHandleState.FAILED
        val exitCondition = AtomicBoolean(false)
        if (waitFor(end, exitCondition, walkCondition) && exitCondition.get()) return PathHandleState.EXIT
        if (!handleConversation()) return PathHandleState.FAILED
        if (waitFor(end, exitCondition, walkCondition) && exitCondition.get()) return PathHandleState.EXIT
        return if (BFSMapCache().canReach(end)) PathHandleState.SUCCESS else PathHandleState.FAILED
    }

    @JvmStatic
    fun handleCharter(destination: CharterShip.Destination?, walkCondition: WalkCondition): PathHandleState {
        if (!CharterShip.openCharterMenu()) return PathHandleState.FAILED;
        val exitCondition = AtomicBoolean(false);
        if (waitFor(destination!!.position, exitCondition, walkCondition) && exitCondition.get()) return PathHandleState.EXIT;
        if (!CharterShip.isInterfaceOpen()) return PathHandleState.FAILED;
        return if(CharterShip.charter(destination)) PathHandleState.SUCCESS else PathHandleState.FAILED;
    }

    fun selectOption(): Boolean {
        val option = getChatOptions().map { obj: Widget -> obj.text }.maxBy {getResponseValue(it) }
        return option != null && processOption(option)
    }

    fun handleConversation(): Boolean {
        processDialog()
        while(isChatDialogOpen() || isOptionsDialogOpen()) {
            if(isOptionsDialogOpen()) {
                selectOption()
            }
            processDialog()
        }
        return true
    }

    private fun waitFor(end: WorldPoint, exitCondition: AtomicBoolean, walkCondition: WalkCondition): Boolean {
        val runManager = RunManager()
        return sleepUntil({
            if (!runManager.isWalking) {
                return@sleepUntil true
            }
            if (walkCondition.asBoolean) {
                exitCondition.set(true)
                return@sleepUntil true
            }
            BFSMapCache().canReach(end)
        }, 12000)
    }

    private fun getResponseValue(text: String?): Int {
        var a = 0
        if (LIKELY.matcher(text).matches()) a++
        if (UNLIKELY.matcher(text).matches()) a--
        if (StrongHoldAnswers.instance.isAnswer(text)) a += 2
        return a
    }
}