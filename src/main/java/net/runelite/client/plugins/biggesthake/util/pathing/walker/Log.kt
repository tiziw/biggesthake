package net.runelite.client.plugins.biggesthake.util.pathing.walker

import org.slf4j.LoggerFactory
import java.util.logging.Level

object Log {
    private val logger = LoggerFactory.getLogger(Log::class.java)
    fun log(warning: Level, server: String, rate_limit_hit: String) {
        logger.info("$warning: $server : $rate_limit_hit")
    }

    fun info(daxWalker: String, format: String) {
        logger.info("$daxWalker: $format")
    }

    fun severe(daxWalker: String, s: String) {
        logger.error("$daxWalker: $s")
    }

    fun fine(s: String?) {
        logger.info(s)
    }
    fun severe(format: String?) {
        logger.error(format)
    }
}