package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

interface Requirement {
    fun satisfies(): Boolean
}