package net.runelite.client.plugins.biggesthake

import net.runelite.api.Client
import net.runelite.client.plugins.biggesthake.api.rs.getMenuBounds
import net.runelite.client.plugins.biggesthake.api.rs.getMenuEntryBounds
import net.runelite.client.plugins.biggesthake.input.getPoint
import net.runelite.client.plugins.biggesthake.util.OverlayProxy
import net.runelite.client.plugins.biggesthake.util.Helper
import java.awt.*
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer
import javax.inject.Inject

class BHOverlay : OverlayProxy() {
    private var rendered = false
    @Inject
    var client: Client? = null

    override fun render(g: Graphics2D): Dimension? {
        if (client!!.isMenuOpen) {
            val canvas = Helper.client.canvas
            rendered = true
            renderWiw(g, getMenuBounds(), Color.PINK)
            for (i in client!!.menuEntries.indices) {
                renderWiw(g, client!!.getMenuEntryBounds(i), Color.GREEN)
            }
        } else {
            rendered = false
        }
        rectangleList.map.forEach { (x, y) ->
            renderWiw(g, x as Rectangle, Color.cyan)
            (y as List<Rectangle>).forEach(Consumer { z: Rectangle -> renderWiw(g, z, Color.RED) })
        }
        return null
    }

    private fun renderWiw(g: Graphics2D, wiw: Rectangle, color: Color) {
        g.color = color
        g.draw(wiw)
    }

    class MyList<T> {
        val map: MutableMap<T, List<Rectangle>>
        fun add(t: T, randomConsumer: Function2<Int, Int, Point>) {
            val l: MutableList<Rectangle> = ArrayList()
            val v = t as Rectangle
            if (v.height * v.width != 1) for (i in 0..999) {
                val p: Point = t.getPoint(randomConsumer)!!
                val r = Rectangle(p.x, p.y, 1, 1)
                l.add(r)
            }
            map[t] = l
            Thread(Runnable {
                try {
                    Thread.sleep(FADE_DELAY.toLong())
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                map.remove(t)
            }).start()
            //do other things you want to do when items are added
        }

        fun remove(t: T): T? {
            map.remove(t)
            return null
        }

        init {
            map = ConcurrentHashMap()
        }
    }

    companion object {
        const val FADE_DELAY = 15000
        var rectangleList: MyList<java.awt.Rectangle> = MyList()
    }
}