package net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions

class RateLimitException(message: String?) : RuntimeException(message)