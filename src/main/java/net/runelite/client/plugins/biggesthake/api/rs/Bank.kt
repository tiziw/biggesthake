package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.widgets.Widget
import net.runelite.api.widgets.WidgetInfo
import net.runelite.client.plugins.biggesthake.api.rs.misc.convert
import net.runelite.client.plugins.biggesthake.api.rs.misc.getLogger
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.enums.InteractOption
import net.runelite.client.plugins.biggesthake.enums.InteractOption.BANK
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.input.getPoint
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import java.awt.event.KeyEvent

object Bank {

    fun isOpen(): Boolean {
        val widget = client.getWidget(WidgetInfo.BANK_CONTENT_CONTAINER)
        return widget != null && widget.isVisible()
    }

    fun depositInventory() {
        if(!isOpen()) {
            throw Exception("Attempted to deposit items with closed bank interface")
        }
        val widget = client.getWidget(WidgetInfo.BANK_DEPOSIT_INVENTORY)
        widget?:throw Exception("Unable to find deposit all widget")
        widget.click()
        sleep(800, 1400)
    }

    fun getBankItems(): Array<out Widget>? {
        sleepUntil({client.getWidget(WidgetInfo.BANK_ITEM_CONTAINER) != null}, 3000, onTimeout = {})
        val widget = client.getWidget(WidgetInfo.BANK_ITEM_CONTAINER)
        widget?:throw Exception("Unable to access inventory widget")
        return client.getWidget(WidgetInfo.BANK_ITEM_CONTAINER).children
    }

    fun depositItem(id: Int = -1, itemName: String = client.getItemDefinition(id).name, amount: Int = -1) {
        val target: Widget? = Bank.getInventoryItems().firstOrNull() { it.name() == itemName }
        if(target == null) { // ${getInventoryItems().map { it.name() }.reduce{a,b -> a + b}}
            getLogger().warn("Failed to find item to deposit");
            return
        }

        val currentAmount = Bank.getInventoryItems().size
        val timeoutTask = {currentAmount < Bank.getInventoryItems().size} // Bank full?
        if(amount == -1) {
            target.interact(action ="Deposit-All", isFinished = timeoutTask)
        } else if(amount == 1 || amount == 5 || amount == 10) {
            target.interact(action = "Deposit-$amount", isFinished = timeoutTask)
        } else {
            target.interact(action = "Deposit-X", isFinished = timeoutTask)
            Input.typeMessage("$amount")
        }
        sleep(1000, 1800)
    }

    fun withdrawItem(id: Int = -1, itemName: String = client.getItemDefinition(id).name, amount: Int = -1) {
        val target: Widget? = getBankItems()?.firstOrNull() { it.name() == itemName }
        if(target == null) {
            getLogger().warn("Failed to find item to deposit");
            return
        }
        val bankContainer = client.getWidget(WidgetInfo.BANK_ITEM_CONTAINER)
        val point = target.bounds.getPoint()
        if(point == null || !bankContainer.contains(point.convert()) ||  !target.isVisible()) { // Not visible
            //TODO: scan tabs
            val button = client.getWidget(WidgetInfo.BANK_SEARCH_BUTTON_BACKGROUND)
            if(button == null) { throw Exception("Unable to click search bank button"); return }
            button.click()
            sleepUntil({ isInputDialogOpen() })

            var index = 0
            val str = itemName.toLowerCase()
            logger.info("Looking for item $str")
            var point = target.bounds.getPoint()
            while((point == null || !bankContainer.contains(point.convert()) || !target.isVisible()) && index < itemName.length) {
                Input.keyPressBlocking(str[index++])
                sleep(100, 250)
                point = target.bounds.getPoint()
            }
            sleep(500, 900)
            target.bounds.getPoint()?:throw Exception("Could not find item in bank")
        }

        // Sometimes goes to here but in interact getPoint returns null
        val currentAmount = Bank.getInventoryItems().size
        val timeoutTask = {currentAmount > Bank.getInventoryItems().size || Inventory.isFull()}
        if(amount == -1) {
            target.interact(action ="Withdraw-All", isFinished = timeoutTask)
        } else if(amount == 1 || amount == 5 || amount == 10) {
            target.interact(action = "Withdraw-$amount", isFinished = timeoutTask)
        } else {
            target.interact(action = "Withdraw-X", isFinished = timeoutTask)
            Input.typeMessage("$amount")
        }
    }

    fun close() {
        if(!isOpen()) {
            return //throw Exception("Attempted to deposit items with closed bank interface")
        }
        Input.keyPressBlocking(KeyEvent.VK_ESCAPE)
    }

    fun open() {
        for(i in 1..2) {
            val bankerNPC = getNearestNPCs().firstOrNull { it.hasAction { action -> action == BANK.asString } }
            if(bankerNPC != null) {
                bankerNPC?.interact(BANK)
                break
            } else {
                val bankObject = getNearestObjects().firstOrNull { it.getName() == "Bank chest" || it.getName() == "Bank booth" }
                bankObject ?: continue
                bankObject.interact { action -> action == InteractOption.USE.asString || action == BANK.asString }
                break
            }
        }
        sleepUntil({Bank.isOpen()}, 10000, onTimeout = {throw Exception("Unable to interact with banker/bank object")})
    }

    fun getInventoryItems(): List<Widget> {
        sleepUntil({client.getWidget(WidgetInfo.BANK_INVENTORY_ITEMS_CONTAINER) != null}, 3000, onTimeout = {})
        val widget = client.getWidget(WidgetInfo.BANK_INVENTORY_ITEMS_CONTAINER)
        widget?:throw Exception("Unable to access bank inventory widget")
        return widget.children.filterNot {it.id == 6512}
    }

    fun depositItems(shouldDeposit: (String) -> Boolean) {
        client.getWidget(WidgetInfo.INVENTORY).widgetItems.distinctBy {it.name()}.forEach {
            if(shouldDeposit(it.name())) {
                depositItem(itemName = it.name())
            }
        }
    }

}