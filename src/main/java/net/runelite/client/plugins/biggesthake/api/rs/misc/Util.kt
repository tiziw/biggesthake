package net.runelite.client.plugins.biggesthake.api.rs.misc

import it.unimi.dsi.util.XoShiRo256PlusPlusRandom
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.input.InputConfig
import java.awt.Point
import java.awt.event.KeyEvent
import java.lang.Thread.sleep
import kotlin.math.sqrt
import kotlin.random.Random

val localRandom: XoShiRo256PlusPlusRandom = XoShiRo256PlusPlusRandom()

fun startResetSeedTimer() {
    GlobalScope.launch {
        while(true) {
            val sleepTime = (70 + Random.high(0, 20))
            delay(1000*60*sleepTime)

            synchronized(localRandom) {
                localRandom.setSeed(localRandom.nextLong())
            }
        }
    }
}

fun Unit.sleep(duration: Number) {
    Thread.sleep(duration.toLong())
}

fun sleep(duration: Number) {
    //TODO: add chance for random mouse movement, maybe checking stats
    Thread.sleep(duration.toLong())
}

fun sleep(min: Number, max:Number) {
    sleep(Random.mid(min, max))
}

fun delay(min: Number, max: Number): () -> Unit {
    return {sleep(min, max)}
}

fun Unit.sleep() {
    Thread.sleep(InputConfig.getMinDelay())
}

fun sleep() {
    Thread.sleep(InputConfig.getMinDelay())
}

fun Random.high(min: Number = 0, max: Number): Long {
    val diff: Double = max.toDouble() - min.toDouble()
    val rand: Double = localRandom.nextDoubleFast() * diff
    return (min.toDouble() + sqrt(diff * diff - rand * rand)).toLong()
}

fun Random.low(min: Number = 0, max: Number): Long {
    val diff: Double = max.toDouble() - min.toDouble();
    val rand: Double = localRandom.nextDoubleFast() * diff
    return (max.toDouble() - sqrt(diff * diff - rand * rand)).toLong()
}

fun Random.mid(min: Number, max: Number): Long {
        val mid: Double = (max.toDouble() - min.toDouble()) / 2.0
        val rand: Double  = localRandom.nextInt(mid.toInt()).toDouble()
        val num: Double  = if (localRandom.nextBoolean()) -1.0 else 1.0
        return ((min.toDouble() + mid) + (num * (mid - sqrt(mid * mid - rand * rand)))).toLong()
}

fun Random.mid(upperBound: Number): Number {
    return Random.mid(0, upperBound)
}

fun Random.high(upperBound: Number): Number {
    return Random.high(0, upperBound)
}

fun Random.low(upperBound: Number): Number {
    return Random.high(0, upperBound)
}

fun Random.random(): DoubleArray {
    val result = DoubleArray(100)

    for (i in result.indices) {
        result[i] = (localRandom.nextDoubleFast() * 100.0).toInt().toDouble()
    }

    return result
}
var goingUp = false
fun rotateCamera(condition: () -> Boolean) {
    val horizonKey = if (Random.nextBoolean()) KeyEvent.VK_LEFT else KeyEvent.VK_RIGHT
    val verticalKey = if (goingUp) KeyEvent.VK_UP else KeyEvent.VK_DOWN
    goingUp = !goingUp

    // Rotate until object is visible
    Input.keyPress(horizonKey)
    Input.keyPress(verticalKey)
    // Clickpoint isn't null if the target is visible
    sleepUntil({ condition() }, 4700, 10, onTimeout = {})
    // Due to threshold/checking speed of 10ms it'll stop when NPC is at edge of screen
    // We sleep a bit longer so its somewhat in center
    Input.keyRelease(verticalKey)
    sleep(Random.mid(700, 900))
    Input.keyRelease(horizonKey)
}

//fun zoomCamera(condition: () -> Rectangle) {
//    val key = if (Random.nextBoolean()) KeyEvent.VK_LEFT else KeyEvent.VK_RIGHT
//    // Rotate until object is visible
//    Input.keyPress(key)
//    // Clickpoint isn't null if the target is visible
//    sleepUntil({ condition().getPoint() != null }, 10, 4700, onTimeout = {})
//    // Due to threshold/checking speed of 10ms it'll stop when NPC is at edge of screen
//    // We sleep a bit longer so its somewhat in center
//    sleep(Random.high(700, 900))
//    Input.keyRelease(key)
//}

fun sleepUntil(condition: () -> Boolean, timeout: Long = Long.MAX_VALUE, threshold: Long = 10, delay: Boolean = true, onTimeout: () -> Any = {throw Exception("Failed to fullfill condition, timeout reached")}): Boolean {
    val startTime = System.currentTimeMillis()
    do {
        if (condition.invoke()) {
            // Reaction time delay
            if(delay)
            sleep(Random.low(250, 330))
            return true;
        }
        sleep(threshold)
    } while (System.currentTimeMillis() - startTime < timeout)
    onTimeout()
    return false;
}

fun sleepRand() {
    sleep(Random.high(90, 167))
}

fun Point.convert(): net.runelite.api.Point {
    return net.runelite.api.Point(this.x, this.y)
}

//fun sleepUntil(condition: () -> Boolean, timeout: Int): Boolean {
//    return sleepUntil(condition, timeout)
//}
