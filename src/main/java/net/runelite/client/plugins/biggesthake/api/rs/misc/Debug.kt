package net.runelite.client.plugins.biggesthake.api.rs.misc

import net.runelite.client.plugins.biggesthake.BHOverlay
import net.runelite.client.plugins.biggesthake.enums.EntityType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.awt.Point
import java.awt.Rectangle

fun addClickRectangle(rect: Rectangle, func: (Int, Int) -> Point) {
    BHOverlay.rectangleList.add(rect, func)
}

fun addClickPoint(x: Int, y: Int) {
    addClickRectangle(Rectangle(x, y, 1, 1), EntityType.GENERIC.randomConsumer)
}

fun Any.getLogger(): Logger {
    return LoggerFactory.getLogger(javaClass)
}
