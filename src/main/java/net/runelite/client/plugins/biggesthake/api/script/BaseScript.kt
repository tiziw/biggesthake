package net.runelite.client.plugins.biggesthake.api.script

import com.google.common.base.Preconditions
import net.runelite.api.Client
import net.runelite.client.plugins.biggesthake.LocalScripts
import net.runelite.client.plugins.biggesthake.api.rs.misc.getLogger
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.pathing.Area
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.RSBank
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.util.*


abstract class BaseScript(private val scriptType: LocalScripts) {
    val logger: Logger = LoggerFactory.getLogger(scriptType.getScriptClass())
    val map: Map<Class<*>, Method> =
            scriptType.getScriptClass().declaredMethods.filter { isEventMethod(it) }.map { Pair(getEventClass(it), it) }.toMap()

    val client: Client = Helper.client
    val params: ScriptParam = ScriptParam(scriptType)
    val queue = Stack<() -> Boolean>()
    var running = true

    fun setParams(area: Area = Area(), autoBank: Boolean = false, keepItems: Array<String> = arrayOf(), preferredBank: RSBank? = null) {
        params.region = area
        params.autoBank = autoBank
        params.keepItems = keepItems.toHashSet()
        params.preferredBank = preferredBank
    }

    val LOOP_DELAY = 150
    fun start() {
        logger.info("Starting script " + scriptType.name)
        verifyStates()
        onStart()

        var lastLoop = System.currentTimeMillis()
        while (running) {
            val diff = System.currentTimeMillis() - lastLoop
            if (diff < LOOP_DELAY) sleep(LOOP_DELAY - diff)

            verifyStates()
            for (i in 1..3) {
                if (run()) break
            }
            lastLoop = System.currentTimeMillis()
        }
    }

    private fun onStart() {
        map.values.forEach {
            val annotation = it.getAnnotation(Event::class.java)
            if (annotation.onStart) {
                it.invoke(this)
            }
        }
    }

    fun run(): Boolean {
        if (!queue.empty()) {
            getLogger().info("Executing run")
            return queue.pop().invoke()
        }
        return true
    }

    private fun verifyStates() {
        val parentResult = params.scriptState.verify()
        val childResult = params.scriptState.verifyChildren()

        // If a state is true and executed a new situation can occur
        // Best to do a new full scan afterwards
        if(parentResult || childResult) verifyStates()
    }

    fun register(event: Any) {
//        map.keys.forEach({ getLogger().info("${it.simpleName} ${it.toString()}") })
//        getLogger().info("Triggered ${event.javaClass.simpleName}")
        if (map.containsKey(event.javaClass)) {
            queue.push { map.get(event.javaClass)?.invoke(this) as Boolean }
        }
    }

    companion object {
        fun isEventMethod(method: Method): Boolean {
            val sub = method.getAnnotation(Event::class.java) ?: return false
            //Preconditions.checkArgument(method.parameterCount == 1, "@Subscribed method \"$method\" must take exactly 1 argument")
            Preconditions.checkArgument(!Modifier.isStatic(method.modifiers), "@Subscribed method \"$method\" cannot be static")
            //val parameterClazz = method.parameterTypes[0]
            //Preconditions.checkArgument(!parameterClazz.isPrimitive, "@Subscribed method \"$method\" cannot subscribe to primitives")
            //Preconditions.checkArgument(parameterClazz.modifiers and (Modifier.ABSTRACT or Modifier.INTERFACE) == 0, "@Subscribed method \"$method\" cannot subscribe to polymorphic classes")
            method.isAccessible = true
            return true
        }

        fun getEventClass(method: Method): Class<*> {
            return method.getAnnotation(Event::class.java).type.java
        }
    }
}
