package net.runelite.client.plugins.biggesthake.core

import net.runelite.api.events.AnimationChanged
import net.runelite.api.events.GameObjectChanged
import net.runelite.client.plugins.biggesthake.api.rs.getObject
import net.runelite.client.plugins.biggesthake.api.rs.interact
import net.runelite.client.plugins.biggesthake.api.rs.isAnimating
import net.runelite.client.plugins.biggesthake.api.rs.localPlayer
import net.runelite.client.plugins.biggesthake.api.rs.misc.delay
import net.runelite.client.plugins.biggesthake.enums.InteractOption.CHOP_DOWN
import net.runelite.client.plugins.biggesthake.util.pathing.Area
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.RSBank
import kotlin.reflect.KClass

class Script(
        var area: Area = Area(),
        var autoBank: Boolean = false,
        var keepItems: Array<String> = arrayOf(),
        var preferredBank: RSBank? = null,
        val tasks: ArrayList<Task> = ArrayList()) {

    fun task(init: Task.() -> Unit): Task {
        val task = Task()
        task.init()
        tasks.add(task)
        return task
    }

}

class Task(
        var method: () -> Boolean = {true},
        var onStart: Boolean = true,
        var events: Array<KClass<*>> = arrayOf(),
        var delay: Array<Int> = arrayOf(800, 1000),
        var preExec: () -> Unit = delay(1000, 2000),
        var condition: () -> Boolean = {true}
)
{}

val wcScript = script {
    area = Area()
    autoBank = true
    task {
        onStart = true
        events = arrayOf(AnimationChanged::class, GameObjectChanged::class)
        condition = {!localPlayer.isAnimating()}
        preExec = delay(1300, 2200)
        method = {
            getObject("Yew tree") {
                it.interact(CHOP_DOWN)
            }
        }
    }
}

//fun method(init: (() -> Boolean).() -> Unit): () -> Boolean {
//    val method = {true}
//    method.init()
//    return method
//}

fun script(init: Script.() -> Unit): Script {
    val script = Script()
    script.init()
    return script
}

//fun task(init: Task.() -> Unit): Task {
//    val task = Task()
//    task.init()
//    return task
//}