package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions



import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.util.pathing.walker.Log
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.BrokenPathHandler
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.EntityHandler.handleCharter
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.EntityHandler.handleWithAction
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.CharterShip
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.LockPickHandler
import java.util.*
import java.util.logging.Level
import java.util.regex.Pattern

class PathLink(val start: WorldPoint, val end: WorldPoint, private val pathLinkHandler: (WorldPoint?, WorldPoint?, WalkCondition?) -> PathHandleState) {
    companion object {
        private var values = ArrayList<PathLink>()
        val KARAMJA_PORT_SARIM = PathLink(WorldPoint(2953, 3146, 0), WorldPoint(3029, 3217, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)pay.fare"), start, end!!, walkCondition!!) }
        val KARAMJA_PORT_PHASMATYS = PathLink(WorldPoint(2953, 3146, 0), WorldPoint(3702, 3503, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_PHASMATYS, walkCondition!!) }
        val PORT_PHASMATYS_KARAMJA = PathLink(WorldPoint(3702, 3503, 0), WorldPoint(2953, 3146, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.MUSA_POINT, walkCondition!!) }
        val PORT_PHASMATYS_PORTSARIM = PathLink(WorldPoint(3702, 3503, 0), WorldPoint(2796, 3414, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_PHASMATYS, walkCondition!!) }
        val PORT_SARIM_PORT_PHASMATYS = PathLink(WorldPoint(2796, 3414, 0), WorldPoint(3702, 3503, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_PHASMATYS, walkCondition!!) }
        val PORT_SARIM_CATHERBY = PathLink(WorldPoint(3041, 3193, 0), WorldPoint(2796, 3414, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.CATHERBY, walkCondition!!) }
        val PORT_SARIM_BRIMHAVEN = PathLink(WorldPoint(3041, 3193, 0), WorldPoint(2760, 3237, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.BRIMHAVEN, walkCondition!!) }
        val PORT_SARIM_KARAMJA = PathLink(WorldPoint(3029, 3217, 0), WorldPoint(2953, 3146, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)pay.fare"), start, end!!, walkCondition!!) }
        val CATHERBY_PORT_SARIM = PathLink(WorldPoint(2796, 3414, 0), WorldPoint(3041, 3193, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_SARIM, walkCondition!!) }
        val CATHERBY_PORT_BRIMHAVEN = PathLink(WorldPoint(2796, 3414, 0), WorldPoint(2760, 3237, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.BRIMHAVEN, walkCondition!!) }
        val CATHERBY_MUSA_POINT = PathLink(WorldPoint(2796, 3414, 0), WorldPoint(2953, 3146, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.MUSA_POINT, walkCondition!!) }
        val CATHERBY_PORT_KHAZARD = PathLink(WorldPoint(2796, 3414, 0), WorldPoint(2673, 3148, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_KHAZARD, walkCondition!!) }
        var BRIMHAVEN_ARDOUGHNE = PathLink(WorldPoint(2772, 3225, 0), WorldPoint(2681, 3275, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)pay.fare"), start, end!!, walkCondition!!) }
        val ARDOUGHNE_BRIMHAVEN = PathLink(WorldPoint(2681, 3275, 0), WorldPoint(2772, 3225, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)pay.fare"), start, end!!, walkCondition!!) }
        val BRIMHAVEN_PORT_SARIM = PathLink(WorldPoint(2760, 3237, 0), WorldPoint(2953, 3146, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_SARIM, walkCondition!!) }
        val KHAZARD_CATHERBY = PathLink(WorldPoint(2673, 3148, 0), WorldPoint(2796, 3414, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.CATHERBY, walkCondition!!) }
        val KHAZARD_PORT_SARIM = PathLink(
                WorldPoint(2673, 3148, 0), WorldPoint(3041, 3193, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_SARIM, walkCondition!!) }
        val PORT_SARIM_KHAZARD = PathLink(WorldPoint(3041, 3193, 0), WorldPoint(2673, 3148, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleCharter(CharterShip.Destination.PORT_KHAZARD, walkCondition!!) }
        val PORT_SARIM_PEST_CONTROL = PathLink(WorldPoint(3041, 3202, 0), WorldPoint(2659, 2676, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)travel"), start, end!!, walkCondition!!) }
        val PEST_CONTROL_PORT_SARIM = PathLink(WorldPoint(2659, 2676, 0), WorldPoint(3041, 3202, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)travel"), start, end!!, walkCondition!!) }
        val PORT_SARIM_PISCARILIUS = PathLink(WorldPoint(3054, 3245, 0), WorldPoint(1824, 3691, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)port pis.+"), start, end!!, walkCondition!!) }
        val PORT_SARIM_LANDS_END = PathLink(WorldPoint(3054, 3245, 0), WorldPoint(1504, 3399, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)land.s end"), start, end!!, walkCondition!!) }
        val LANDS_END_PISCARILIUS = PathLink(WorldPoint(1504, 3399, 0), WorldPoint(1824, 3691, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)port pis.+"), start, end!!, walkCondition!!) }
        val LANDS_END_PORT_SARIM = PathLink(WorldPoint(1504, 3399, 0), WorldPoint(3054, 3245, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)port sarim"), start, end!!, walkCondition!!) }
        val PISCARILIUS_LANDS_END = PathLink(WorldPoint(1824, 3691, 0), WorldPoint(1504, 3399, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)land.s end"), start, end!!, walkCondition!!) }
        val PISCARILIUS_PORT_SARIM = PathLink(
                WorldPoint(1824, 3691, 0), WorldPoint(3054, 3245, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> handleWithAction(Pattern.compile("(?i)port sarim"), start, end!!, walkCondition!!) }
        val LUMBRIDGE_HAM_HIDEOUT = PathLink(WorldPoint(3166, 3251, 0), WorldPoint(3149, 9652, 0)) { position: WorldPoint?, destination: WorldPoint?, walkCondition: WalkCondition? -> LockPickHandler.handle(position!!, destination!!, walkCondition!!) }
        val HAM_JAIL = PathLink(WorldPoint(3183, 9611, 0), WorldPoint(3182, 9611, 0)) { position: WorldPoint?, destination: WorldPoint?, walkCondition: WalkCondition? -> LockPickHandler.handle(position!!, destination!!, walkCondition!!) }
        val BURTHORP_DOWNSTAIRS = PathLink(WorldPoint(2899, 3565, 0), WorldPoint(2205, 4934, 1)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> if (BrokenPathHandler.NextMove.FLOOR_UNDER.handle()) PathHandleState.SUCCESS else PathHandleState.FAILED }
        val BURTHORP_UPSTAIRS = PathLink(WorldPoint(2205, 4934, 1), WorldPoint(2899, 3565, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> if (BrokenPathHandler.NextMove.FLOOR_ABOVE.handle()) PathHandleState.SUCCESS else PathHandleState.FAILED }
        val PORT_PHASMATYS_SOUTH = PathLink(WorldPoint(3659, 3507, 0), WorldPoint(3659, 3509, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> if (BrokenPathHandler.NextMove.SAME_FLOOR.handle()) PathHandleState.SUCCESS else PathHandleState.FAILED }
        val DEATH_PLATEAU_DUNGEON = PathLink(WorldPoint(2858, 3577, 0), WorldPoint(2269, 4752, 0)
        ) { start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition? -> if (BrokenPathHandler.NextMove.SAME_FLOOR.handle()) PathHandleState.SUCCESS else PathHandleState.FAILED }

        fun getValues(): List<(PathLink)>? {
            return values
        }

        init {
            values.add(KARAMJA_PORT_SARIM)
            values.add(KARAMJA_PORT_PHASMATYS)
            values.add(PORT_PHASMATYS_KARAMJA)
            values.add(PORT_PHASMATYS_PORTSARIM)
            values.add(PORT_SARIM_PORT_PHASMATYS)
            values.add(PORT_SARIM_CATHERBY)
            values.add(PORT_SARIM_BRIMHAVEN)
            values.add(PORT_SARIM_KARAMJA)
            values.add(CATHERBY_PORT_SARIM)
            values.add(CATHERBY_PORT_BRIMHAVEN)
            values.add(CATHERBY_MUSA_POINT)
            values.add(CATHERBY_PORT_KHAZARD)
            values.add(BRIMHAVEN_ARDOUGHNE)
            values.add(ARDOUGHNE_BRIMHAVEN)
            values.add(BRIMHAVEN_PORT_SARIM)
            values.add(KHAZARD_CATHERBY)
            values.add(KHAZARD_PORT_SARIM)
            values.add(PORT_SARIM_KHAZARD)
            values.add(PORT_SARIM_PEST_CONTROL)
            values.add(PEST_CONTROL_PORT_SARIM)
            values.add(PORT_SARIM_PISCARILIUS)
            values.add(PORT_SARIM_LANDS_END)
            values.add(LANDS_END_PISCARILIUS)
            values.add(LANDS_END_PORT_SARIM)
            values.add(PISCARILIUS_LANDS_END)
            values.add(PISCARILIUS_PORT_SARIM)
            values.add(LUMBRIDGE_HAM_HIDEOUT)
            values.add(HAM_JAIL)
            values.add(BURTHORP_DOWNSTAIRS)
            values.add(BURTHORP_UPSTAIRS)
            values.add(PORT_PHASMATYS_SOUTH)
            values.add(DEATH_PLATEAU_DUNGEON)
        }
    }

    fun handle(walkCondition: WalkCondition?): PathHandleState? {
        Log.log(Level.FINE, "DaxWalker", "Triggering $this")
        return this.pathLinkHandler.invoke(start, end, walkCondition)
    }

}