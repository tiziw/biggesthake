package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.localPlayer
import net.runelite.client.plugins.biggesthake.api.rs.misc.*
import net.runelite.client.plugins.biggesthake.api.rs.walkTo
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.pathing.walker.Log
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.BrokenPathHandler.handle
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.BrokenPathHandler.handlePathLink
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathHandleState
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathLink
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.Teleport
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding.BFSMapCache
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding.Region
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.Movement
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.RunManager
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.ShipHandler.isOnShip
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.ShipHandler.offBoat
import java.util.concurrent.atomic.AtomicBoolean
import java.util.logging.Level
import kotlin.random.Random


object PathHandler {
    /**
     * @param path          >= 1 in size
     * @param walkCondition exit condition/ passive actions
     * @param maxRetries       max retries on fail
     * @return true if path completed successfully.
     */
    fun walk(path: List<WorldPoint>, walkCondition: WalkCondition, maxRetries: Int, pathLinks: List<PathLink?>?): Boolean {
        if (!handleTeleports(path)) {
            Log.log(Level.WARNING, "DaxWalker", "Failed to handle teleports...")
            return false
        }
        var fail = 0
        var previous: WorldPoint? = null
//        Log.log(Level.INFO, "DaxWalker", "Path is ${path.size} long, with first ${path.get(0).toString()} and ${path.get(1).toString()}")
        loop@ while (!isFinishedPathing(path[path.size - 1])) {

// NO support yet for pausing
//            if (script.isPaused()) {
//                Time.sleep(500);
//                continue;
//            }
            sleep(50)
            if (fail >= maxRetries) return false
            val next = furthestTileInPath(path, randomDestinationDistance())
//            getLogger().info("Dist betw tile (${localPlayer.worldLocation.distanceTo2D(next)})")
            when (handleNextAction(previous, next, path, walkCondition, pathLinks)) {
                PathHandleState.SUCCESS -> {
                    previous = next
                    fail = 0
                }
                PathHandleState.FAILED -> {
                    fail++
                    previous = next
                    try {
                        Thread.sleep(2500, 3500)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    continue@loop
                }
                //TODO: Make sure this is right
                PathHandleState.EXIT -> return true
            }
        }
        return true
    }

    fun getFurthestReachableTileInMinimap(path: List<WorldPoint>): WorldPoint? {
        val reversed = path.reversed()
        for (loc in reversed) {
            if (loc.isInScene(Helper.client) && loc.withinMinimap()) {
                return loc
            }
        }
        return null
    }

    private fun handleNextAction(previous: WorldPoint?, now: WorldPoint?, path: List<WorldPoint>, walkCondition: WalkCondition, pathLinks: List<PathLink?>?): PathHandleState {
        if (isOnShip) return if (offBoat) PathHandleState.SUCCESS else PathHandleState.FAILED
        if (now == null) return PathHandleState.FAILED
        // Disconnected Path
        if (previous != null && previous == now) {
            val next = getNextTileInPath(now, path)
            if (next != null) {
                Log.log(Level.FINE, "DaxWalker", String.format("Disconnected path: (%d,%d,%d) -> (%d,%d,%d)",
                        now.x, now.y, now.plane,
                        next.x, next.y, next.plane
                ))
                val pathHandleState = handlePathLink(now, next, walkCondition, pathLinks!!)
                return pathHandleState ?: handle(now, next, walkCondition!!)
            }
            Log.log(Level.FINE, "DaxWalker", "Clicking supposed last tile in path...")
            return if (now.walkTo()) PathHandleState.SUCCESS else PathHandleState.FAILED // Finished Path most likely
        }
        val destination = BFSMapCache(now, Region()).getRandom(2)

        // Normal Walking
        getLogger().info("Dist rand tile (${localPlayer.worldLocation.distanceTo2D(destination)})")
        if (!destination.walkTo()) return PathHandleState.FAILED
        val exitCondition = AtomicBoolean(false)
        val runManager = RunManager()
        val distance = randomWaitDistance()
        sleepUntil({
            if (walkCondition.asBoolean) {
                exitCondition.set(true)
                return@sleepUntil true
            }
            !runManager.isWalking || localPlayer.worldLocation.distanceTo2D(destination) <= distance
        }, 15000)
        return if (exitCondition.get()) PathHandleState.EXIT else PathHandleState.SUCCESS
    }

    private fun isFinishedPathing(destination: WorldPoint): Boolean {
        if (localPlayer.worldLocation == destination) return true
        val walkingTo = Movement.destination ?: return false
        return if (walkingTo == destination) true else walkingTo.distanceTo2D(destination) < 5 && BFSMapCache(walkingTo, Region()).getMoveCost(destination) <= 2
    }

    private fun getNextTileInPath(current: WorldPoint, path: List<WorldPoint>): WorldPoint? {
        for (i in 0 until path.size - 1) {
            if (path[i] == current) return path[i + 1]
        }
        return null
    }

    fun furthestTileInPath(path: List<WorldPoint>, limit: Int): WorldPoint? {
        val playerWorldPoint = localPlayer.worldLocation

        val bfsMapCache = BFSMapCache(playerWorldPoint, Region())
        for (i in path.indices.reversed()) {
            //getLogger().info("Checking location ${path[i].toString()} against player ${playerWorldPoint.toString()}")
            if (path[i].plane != playerWorldPoint.plane || bfsMapCache.isBlocked(path[i], limit)) continue // && bfsMapCache.getMoveCost(path[i]) <= limit
            if (path[i].distanceTo2D(playerWorldPoint) <= limit) return path[i]
        }
        return null
    }

    private fun handleTeleports(path: List<WorldPoint>): Boolean {
        val startWorldPoint = path[0]
        val playerWorldPoint = localPlayer.worldLocation
        for (teleport in Teleport.values()) {
            if (!teleport.requirement.invoke()) continue
            if (!teleport.isAtTeleportSpot(playerWorldPoint) && teleport.isAtTeleportSpot(startWorldPoint)) {
                Log.fine("Let use: $teleport")
                if (!teleport.trigger()) return false
                if (!sleepUntil({ teleport.isAtTeleportSpot(localPlayer.worldLocation) }, 8000)) return false
                Log.log(Level.FINE, "Teleport", "Using teleport: $teleport")
                break
            }
        }
        return true
    }

    private fun randomWaitDistance(): Int {
        return Random.mid(4, 15).toInt()
    }

    private fun randomDestinationDistance(): Int {
        return Random.mid(13, 16).toInt()
    }
}