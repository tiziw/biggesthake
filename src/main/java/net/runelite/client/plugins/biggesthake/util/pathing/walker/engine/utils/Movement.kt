package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.click
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client

object Movement {
    @kotlin.jvm.JvmStatic
    val destination: WorldPoint?
        get() = if(client.localDestinationLocation==null) null; else
                WorldPoint.fromLocal(client, client.localDestinationLocation)
    val isRunEnabled: Boolean
        get() = client.varps[173] == 1


    @JvmStatic
    val runEnergy: Int
        get() = client.energy

    @JvmStatic
    fun toggleRun(b: Boolean) {
        val w = client.getWidget(160, 22)
        w.click()
    }
}