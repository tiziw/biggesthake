package net.runelite.client.plugins.biggesthake.util

import net.runelite.api.Client
import net.runelite.client.Notifier
import net.runelite.client.callback.ClientThread
import net.runelite.client.ui.ClientPluginToolbar
import net.runelite.client.ui.ClientUI
import net.runelite.client.ui.ContainableFrame
import net.runelite.client.ui.PluginPanel
import org.apache.commons.lang3.reflect.FieldUtils
import org.slf4j.LoggerFactory
import java.awt.Canvas
import java.util.concurrent.CompletableFuture

class Helper {

    companion object {
        private val logger = LoggerFactory.getLogger(Helper::class.java)

        @get:Synchronized
        lateinit var clientUI: ClientUI
        lateinit var client: Client
        lateinit var clientThread: ClientThread
        lateinit var notifier: Notifier


        lateinit var frame: ContainableFrame
        var pluginPanel: PluginPanel? = null
        var pluginToolbar: ClientPluginToolbar? = null

        fun init(client: Client, clientUI: ClientUI, clientThread: ClientThread, notifier: Notifier) {
            Companion.client = client;
            Companion.clientUI = clientUI
            Companion.clientThread = clientThread
            Companion.notifier = notifier
            loadReflectionFields()
        }

        private fun loadReflectionFields() {
            try {
                frame = FieldUtils.readField(clientUI, "frame", true) as ContainableFrame
                logger.info("Detected RuneLite client")
            } catch (e: Exception) {
                e.printStackTrace()
                logger.error("Unable to read client UI fields!")
            }
        }

        fun runMainThread(task: () -> Any): Any {
            val future = CompletableFuture<Any>()
            clientThread.invoke( Runnable { future.complete(task()) })
            // Busy-waits until the future has been completed
            return future.get()
        }

        val canvas: Canvas
            get() {
                return client.canvas
            }

    }
}