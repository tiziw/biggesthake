package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils

import net.runelite.api.GameObject
import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.BrokenPathHandler
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathHandleState
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import java.util.concurrent.atomic.AtomicBoolean
import java.util.regex.Pattern

object LockPickHandler {
    private val PICK_LOCK_ACTION = Pattern.compile("(?i)pick.*lock")
    private val HAM_TRAPDOOR = WorldPoint(3166, 3251, 0)
    fun handle(position: WorldPoint, destination: WorldPoint, walkCondition: WalkCondition): PathHandleState {
        return if (position == HAM_TRAPDOOR) hamHideOutHandler(position, destination, walkCondition) else pickLock(position, destination, walkCondition)
    }

    private fun getPickableObject(position: WorldPoint): GameObject? {
        return null
        //        return Arrays.stream(SceneObjects.getLoaded(sceneObject -> sceneObject.containsAction(s -> s.matches("(?i)pick.*lock"))))
//                .min(Comparator.comparingDouble(o -> o.distance(position)))
//                .orElse(null);
    }

    private fun pickLock(position: WorldPoint, destination: WorldPoint, walkCondition: WalkCondition): PathHandleState { //        SceneObject sceneObject = getPickableObject(position);
//        if (sceneObject == null) return PathHandleState.FAILED;
//
//        // Pick the lock
//        if (!sceneObject.interact(s -> PICK_LOCK_ACTION.matcher(s).matches())) return PathHandleState.FAILED;
//        AtomicBoolean exitCondition = new AtomicBoolean(false);
//        if (Time.sleepUntil(() -> {
//            if (walkCondition.getAsBoolean()) {
//                exitCondition.set(true);
//                return true;
//            }
//            return destination.equals(localPlayer.getWorldLocation());
//        }, Random.nextInt(800, 2000)) && exitCondition.get()) {
//            return PathHandleState.EXIT;
//        }
        return PathHandleState.SUCCESS // Multiple attempts required usually.
    }

    private fun hamHideOutHandler(position: WorldPoint, destination: WorldPoint, walkCondition: WalkCondition): PathHandleState {
        val sceneObject = getPickableObject(position)
                ?: return BrokenPathHandler.handle(position, destination, walkCondition)
        // Lock already picked
        // Pick the lock
//        if (!sceneObject.interact(s -> PICK_LOCK_ACTION.matcher(s).matches())) return PathHandleState.FAILED;
        val exitCondition = AtomicBoolean(false)
        //        if (UtilKt.sleepUntil(() -> {
//            if (walkCondition.getAsBoolean()) {
//                exitCondition.set(true);
//                return true;
//            }
//            return destination.equals(localPlayer.getWorldLocation()) || getPickableObject(position) == null;
//        }, 15000) && exitCondition.get()) {
//            return PathHandleState.EXIT;
//        }
//        return destination.equals(localPlayer.getWorldLocation()) ? PathHandleState.SUCCESS : PathHandleState.FAILED;
        return PathHandleState.SUCCESS
    }
}