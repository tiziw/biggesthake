package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.widgets.Widget
import net.runelite.api.widgets.WidgetInfo
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.awt.event.KeyEvent

val logger: Logger = LoggerFactory.getLogger("ChatDialog")

fun isInputDialogOpen(): Boolean {
    val widget: Widget? = client.getWidget(WidgetInfo.CHATBOX_CONTAINER)
    return widget != null && !widget.isSelfHidden
}

fun isChatDialogOpen(): Boolean {
    val widget: Widget? = client.getWidget(WidgetInfo.CHATBOX_MESSAGES.groupId, 561)
    return widget != null && !widget.isSelfHidden
}

fun isOptionsDialogOpen(): Boolean {
    val widget: Widget? = client.getWidget(WidgetInfo.DIALOG_OPTION)
    return widget != null && widget.isVisible()
}

fun processDialog() : Boolean {
    sleepUntil({ isChatDialogOpen() }, 3000)

    if(!isChatDialogOpen()) {
        logger.warn("Trying to process dialog, while it isn't open")
        return false
    }
    Input.keyPress(KeyEvent.VK_SPACE)
    sleepUntil({ !isChatDialogOpen() || isOptionsDialogOpen() },  5000, 50)
    Input.keyRelease(KeyEvent.VK_SPACE)
    return true
}

fun getContinueDialogText(): String? {
    if(client.getWidget(WidgetInfo.DIALOG_NPC) != null) {
        return client.getWidget(WidgetInfo.DIALOG_NPC_CONTINUE)?.text
    } else if(client.getWidget(WidgetInfo.DIALOG_PLAYER) != null){
        return client.getWidget(WidgetInfo.DIALOG_PLAYER).staticChildren[2]?.text
    }
    return null
}

fun getChatOptions(): Array<out Widget> {
    sleepUntil({
        val widget: Widget? = client.getWidget(WidgetInfo.DIALOG_OPTION.groupId, 1)
        widget != null && !widget.isSelfHidden
    }, 3000)

    val widget: Widget? = client.getWidget(WidgetInfo.DIALOG_OPTION.groupId, 1)
    widget?:throw Exception("Can't process option, while dialog isn't open")

    return widget.children
}

fun processOption(option: String = "", condition: (String) -> Boolean = { it == option }): Boolean {
    sleepUntil({
        isOptionsDialogOpen()
    }, 3000)

    val widget: Widget? = client.getWidget(WidgetInfo.DIALOG_OPTION.groupId, 1)
    if(widget == null) {
        logger.warn("Can't process option, while dialog isn't open")
        return false
    }

    val option = widget.children.find{ condition.invoke(it.text) }
    if(option == null) {
        logger.warn("Can't choose option, option wasn't found")
        return false;
    }

    Input.keyPressBlocking("${option.index}"[0])
    sleepUntil({
        val dialog = client.getWidget(WidgetInfo.DIALOG_OPTION.groupId, 1)
        dialog == null || dialog.isSelfHidden || widget.isSelfHidden || option.text != ("Please wait...")
    }, 5000)

    return true
}