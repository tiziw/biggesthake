package net.runelite.client.plugins.biggesthake

import net.runelite.client.plugins.biggesthake.api.script.BaseScript
import net.runelite.client.plugins.biggesthake.scripts.TestScript
import net.runelite.client.plugins.biggesthake.scripts.Woodcutter
import kotlin.reflect.KClass

class DummyScript: BaseScript(LocalScripts.NONE) {}

enum class LocalScripts(name: String, private val scriptKClass: KClass<*>) {
    TEST_1("Test1", TestScript::class),
    WC("Tree cutter", Woodcutter::class),
    NONE("None", DummyScript::class);

    fun getScriptClass(): Class<out Any> {
        return scriptKClass.java
    }

    fun newInstance(): BaseScript {
        return getScriptClass().getConstructor().newInstance() as BaseScript
    }

    override fun toString(): String {
        return name
    }
}
