package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.pathing.walker.DaxWalker
import net.runelite.client.plugins.biggesthake.util.pathing.walker.Log
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.Teleport
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding.BFSMapCache
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.RunEnergyManager
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.PathResult
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.PathStatus
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.Point3D
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.logging.Level
import java.util.stream.Collectors

class WalkerEngine(walkCondition: WalkCondition?, private val instance: DaxWalker) {
    private val map: MutableMap<WorldPoint?, Teleport>
    private var walkCondition: WalkCondition
    private val runEnergyManager: RunEnergyManager
    // DEBUG PAINT
    private var lastPath: PathResult? = null
    private val bfsMapCache: BFSMapCache? = null
    private val playerWorldPoint: WorldPoint? = null
    @JvmOverloads
    fun walk(list: List<PathResult?>?, walkCondParam: WalkCondition? = null): Boolean {
        walkCondition = walkCondParam ?: WalkCondition { false }
        //walkCondition = if (walkCondition != null) this.walkCondition.or( walkCondition) else this.walkCondition
        //Game.getEventDispatcher().register(this);
        return try {
            val validPaths = validPaths(list)
            val pathResult = getBestPath(validPaths)
            if (pathResult == null) {
                Log.log(Level.WARNING, "DaxWalker", "No valid path found")
                return false
            }
            lastPath = pathResult
            Log.log(Level.FINE, "DaxWalker", String.format("Chose path of cost: %d out of %d options.", pathResult.cost, validPaths.size))
            return PathHandler.walk(convert(pathResult.path), walkCondition, 3, instance.store.pathLinks)
        } finally {

        }
    }

    fun validPaths(list: List<PathResult?>?): List<PathResult?> {
        val result = list!!.stream().filter { pathResult: PathResult? -> pathResult != null && pathResult.pathStatus == PathStatus.SUCCESS }.collect(Collectors.toList())
        if (result.isNotEmpty()) {
            return result
        }
        if (list.stream().anyMatch { pathResult: PathResult? -> pathResult?.pathStatus == PathStatus.BLOCKED_END }) {
            Log.severe("DaxWalker", "Destination is not walkable.")
        }
        return emptyList()
    }

    fun getBestPath(list: List<PathResult?>): PathResult? {
        return list.minBy {getPathMoveCost(it!!) }
    }

    fun setWalkCondition(walkCondition: WalkCondition?) {
        this.walkCondition = walkCondition!!
    }

    fun andWalkCondition(walkCondition: WalkCondition) {
        this.walkCondition!!.and(walkCondition)
    }

    fun orWalkCondition(walkCondition: WalkCondition) {
        this.walkCondition!!.or(walkCondition)
    }

    private fun convert(list: List<Point3D?>?): List<WorldPoint> {
        val positions: MutableList<WorldPoint> = ArrayList()
        for (point3D in list!!.filterNotNull()) {
            positions.add(WorldPoint(point3D.x, point3D.y, point3D.z))
        }
        return positions
    }

    private fun getPathMoveCost(pathResult: PathResult): Int {
        if (Helper.client.localPlayer!!.worldLocation == pathResult.path?.get(0)?.toWorldPoint()) return pathResult.cost
        val teleport = map[pathResult.path?.get(0)?.toWorldPoint()] ?: return pathResult.cost
        return teleport.moveCost + pathResult.cost
    }

    init {
        map = ConcurrentHashMap()
        for (teleport in Teleport.values()) {
            map[teleport.location] = teleport
        }
        runEnergyManager = RunEnergyManager()
        this.walkCondition = WalkCondition {
            runEnergyManager.trigger()
            false
        }
        if (walkCondition != null) {
            this.walkCondition = this.walkCondition!!.and(walkCondition)
        }
    }
}