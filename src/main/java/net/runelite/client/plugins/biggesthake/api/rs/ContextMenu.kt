package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.Client
import net.runelite.api.MenuEntry
import net.runelite.client.plugins.biggesthake.api.rs.misc.low
import net.runelite.client.plugins.biggesthake.api.rs.misc.mid
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import net.runelite.client.plugins.biggesthake.util.font.FontUtil.stringWidth
import net.runelite.client.util.Text
import java.awt.Point
import java.awt.Rectangle
import kotlin.math.min
import kotlin.math.sqrt
import kotlin.random.Random

// Context menu bounds, updated during getMenuEntryBounds
var bounds: Rectangle = Rectangle()

fun MenuEntry.targetName(): String {
    return Text.removeTags(this.target)
}

fun getMenuBounds(): Rectangle {
    return bounds
}

fun getMenuClickPoint(width: Int, height: Int): Point {
    val mouseX = Helper.canvas.mousePosition.x

    val diff = 35 + min(0, mouseX - bounds.x)
    val sqrt = sqrt(diff.toDouble())
    var rand = Random.mid(diff-sqrt, diff + sqrt)

    if(Random.nextInt(0, 10) == 0) {
        rand = Random.mid(0, diff*2)
    } else if(Random.nextInt(0, 12) == 1) {
        rand = Random.low(0, width)
    }

    return Point(rand.toInt(), Random.mid(height).toInt())
}

/**
 * Coords are already stretched, no conversion should happen again
 */
fun Client.getMenuEntryBounds(index: Int): Rectangle {
    calculateMenuBounds()
    return Rectangle(bounds.x, bounds.y +((client.menuEntries.size - index)*15) + 5, 10,11)
}

private val regex = Regex("(?i)<col[^>]*>")

fun calculateMenuBounds() {
    var menuWidth = stringWidth("Choose Option")
    var menuHeight = 0
    var menuY = 0
    var menuX = 0

    val mouseX = client.mouseCanvasPosition.x
    val mouseY = client.mouseCanvasPosition.y

    var count: Int = 0
    while (count < client.menuEntries.size) {
        val entry = client.menuEntries[count]
        val entryStr = Text.removeTags("${entry.option} ${entry.target}")
        menuX = stringWidth(entryStr)
        if (menuX > menuWidth) {
            menuWidth = menuX
        }
        ++count
    }

    menuWidth += 8
    count = client.menuEntries.size * 15 + 22
    menuX = mouseX - menuWidth / 2


    if (menuX + menuWidth > client.canvasWidth) {
        menuX = client.canvasWidth - menuWidth
    }
    if (menuX < 0) {
        menuX = 0
    }
    menuY = mouseY
    if (mouseY + count > client.canvasHeight) {
        menuY = client.canvasHeight - count
    }
    if (menuY < 0) {
        menuY = 0
    }

    menuHeight = client.menuEntries.size * 15 + 22

    bounds.x = menuX
    bounds.y = menuY
    bounds.width = menuWidth
    bounds.height = menuHeight
}
