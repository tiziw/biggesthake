package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

enum class WalkState {
    FAILED, SUCCESS, START_BLOCKED, END_BLOCKED, RATE_LIMIT, ERROR
}