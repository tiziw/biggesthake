package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.widgets.Widget
import net.runelite.api.widgets.WidgetInfo
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import net.runelite.client.util.Text

fun isWidgetOpen(groupId: Int, component: Int): Boolean {
    val container = client.getWidget(groupId, component).parent.children
    return component >= 0 && container != null && container.isNotEmpty() && container.size >= component && container[component] != null;
}

fun isWidgetOpen(widgetInfo: WidgetInfo): Boolean {
    val widget = client.getWidget(widgetInfo)
    return widget != null && widget.isVisible()
}

fun Widget.isVisible(): Boolean {
    return Helper.runMainThread { !this.isHidden } as Boolean
}

fun Widget.name(): String {
    return Text.removeTags(this.name)
}