package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

class BankPathRequestPair(val start: Point3D, val bank: RSBank?)