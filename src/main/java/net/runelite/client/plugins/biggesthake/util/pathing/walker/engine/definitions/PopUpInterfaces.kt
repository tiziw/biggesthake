package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions

import net.runelite.api.widgets.Widget
import net.runelite.client.plugins.biggesthake.api.rs.click
import net.runelite.client.plugins.biggesthake.api.rs.isVisible
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.util.Helper

enum class PopUpInterfaces(private val rsPopUp: () -> Widget) {
    STRONGHOLD_PROMPT({ Helper.client.getWidget(579, 17) }), WILDERNESS_PROMPT( { Helper.client.getWidget(475, 11) });

    fun handle(): Boolean {
        val interfaceComponent = rsPopUp.invoke() ?: return false
        if (!interfaceComponent.click()) return false
        sleep(450, 1000)
        return true
    }

    val isVisible: Boolean
        get() {
            val interfaceComponent: Widget? = rsPopUp.invoke()
            return interfaceComponent != null && interfaceComponent.isVisible()
        }

    companion object {
        fun resolve(): Boolean {
            for (popUpInterfaces in values()) {
                if (!popUpInterfaces.isVisible) continue
                return popUpInterfaces.handle()
            }
            return false
        }
    }

}