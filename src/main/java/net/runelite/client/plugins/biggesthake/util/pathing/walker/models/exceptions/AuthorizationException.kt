package net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions

class AuthorizationException(message: String?) : RuntimeException(message)