package net.runelite.client.plugins.biggesthake.api.script

import net.runelite.api.GameState
import net.runelite.api.widgets.WidgetInfo.LOGIN_CLICK_TO_PLAY_SCREEN
import net.runelite.client.plugins.biggesthake.api.rs.isWidgetOpen
import net.runelite.client.plugins.biggesthake.api.rs.misc.getLogger
import net.runelite.client.plugins.biggesthake.api.rs.misc.low
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import java.util.*
import kotlin.random.Random

data class StateObject(val stateVerifier: () -> Boolean, val stateExecutor: () -> Unit, val stateName: String, val isEnum: Boolean = false, val parent: StateObject? = null) : () -> Boolean {
    private val dependencies: MutableList<StateObject> = if (!isEnum) engineDependencies() else ArrayList()
    private val children: MutableList<StateObject> = ArrayList()

    init {
        parent?.addChild(this)
    }

    fun addChild(it: StateObject) {
        children.add(it)
    }

    override fun invoke(): Boolean {
        return stateVerifier.invoke()
    }

    fun verify(): Boolean {
        dependencies.forEach { it.verify(); it.verifyChildren() }
        if (invoke()) {
            getLogger().info("Entering state: " + this.stateName)
            sleep(Random.low(250, 330))
            stateExecutor.invoke()
            return true
        }
        return false
    }

    fun verifyChildren(): Boolean {
        return children.any { it.verify() || it.verifyChildren() }
    }


}

fun engineDependencies(): MutableList<StateObject> {
    return EngineState.values().map { it.state }.toMutableList()
}

// State verifier invocation returns whether the
// Game is in the current state.
// If so, run the State Executor to leave it

// Engine state can occur anytime, even when a script is being executed
enum class EngineState(val state: StateObject) {
    NOT_LOGGED_IN(StateObject({ client.gameState != GameState.LOGGED_IN || isWidgetOpen(LOGIN_CLICK_TO_PLAY_SCREEN) }, { BHEngine.handleLogin() }, "Handling login", isEnum = true));

    //constructor(stateVerifier: () -> Boolean, stateExecutor: () -> Unit) : this(StateObject(stateVerifier, stateExecutor, isEnum = true, stateName = ))
}

// Event state can also happen any time, however it's directly triggered
// Using an event, so requires no busy waiting
enum class EventState(val state: StateObject) {;

    //constructor(stateVerifier: () -> Boolean, stateExecutor: () -> Unit) : this(StateObject(stateVerifier, stateExecutor, isEnum = true))
}
