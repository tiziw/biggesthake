package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

enum class TileFlag(private vararg val flags: Int) {
    BLOCKED(0x100, 0x20000, 0x200000, 0xFFFFFF), BLOCKED_NORTH(0x2, 0x400), BLOCKED_EAST(0x8, 0x1000), BLOCKED_SOUTH(0x20, 0x4000), BLOCKED_WEST(0x80, 0x10000), INITIALIZED(0x1000000);

    fun valid(flag: Int): Boolean {
        for (f in flags) {
            if (f and flag == f) return true
        }
        return false
    }
}