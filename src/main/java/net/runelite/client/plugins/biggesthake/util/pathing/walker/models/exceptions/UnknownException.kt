package net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions

class UnknownException(message: String?) : RuntimeException(message)