package net.runelite.client.plugins.biggesthake.core

import net.runelite.client.plugins.biggesthake.BHConfiguration
import net.runelite.client.plugins.biggesthake.LocalScripts
import net.runelite.client.plugins.biggesthake.api.script.BaseScript
import java.lang.reflect.Method
import java.util.*
import kotlin.collections.HashMap

class ScriptManager {
    companion object {
        private var threadGroup = ThreadGroup("Manager")
        private val runningThreads = ArrayList<Thread>()
        private var scriptThread: Thread = Thread()

        val map: HashMap<Class<*>, Method> = HashMap()

        var currentScript: BaseScript? = null
        var config: BHConfiguration? = null

        val scriptEnum: LocalScripts
            get() {
                return config?.script()!!
            }

        fun init(bhConfig: BHConfiguration?) {
            config = bhConfig
        }

        fun start() {
            try {
                //BHEngine.handleLogin()
                currentScript = scriptEnum.newInstance()
                scriptThread = Thread(threadGroup) {
                    currentScript?.start()
                }.apply { start() }
                runningThreads.add(scriptThread)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun running(): Boolean {
            return currentScript?.running!!
        }

        fun stop() {
            currentScript?.running = false
            scriptThread.stop()
            threadGroup.destroy()
            threadGroup = ThreadGroup("Manager")
            runningThreads.clear()
        }
    }
}