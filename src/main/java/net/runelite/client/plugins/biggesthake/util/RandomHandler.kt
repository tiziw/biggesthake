package net.runelite.client.plugins.biggesthake.util

import com.google.common.collect.ImmutableSet
import net.runelite.api.NPC
import net.runelite.api.NpcID.*
import net.runelite.client.plugins.biggesthake.api.rs.interact
import net.runelite.client.plugins.biggesthake.api.rs.isChatDialogOpen
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.api.rs.processDialog
import net.runelite.client.plugins.biggesthake.enums.InteractOption
import net.runelite.client.plugins.biggesthake.input.GuiHelper.Companion.notify

object RandomHandler {
    val EVENT_NPCS: Set<Int> = ImmutableSet.of(
            DR_JEKYLL, DR_JEKYLL_314,
            BEE_KEEPER_6747,
            CAPT_ARNAV,
            SERGEANT_DAMIEN_6743,
            DRUNKEN_DWARF,
            FREAKY_FORESTER_6748,
            GENIE, GENIE_327,
            EVIL_BOB, EVIL_BOB_6754,
            POSTIE_PETE_6738,
            LEO_6746,
            MYSTERIOUS_OLD_MAN_6750, MYSTERIOUS_OLD_MAN_6751,
            MYSTERIOUS_OLD_MAN_6752, MYSTERIOUS_OLD_MAN_6753,
            PILLORY_GUARD,
            FLIPPA_6744,
            QUIZ_MASTER_6755,
            RICK_TURPENTINE, RICK_TURPENTINE_376,
            SANDWICH_LADY,
            DUNCE_6749,
            NILES, NILES_5439,
            MILES, MILES_5440,
            GILES, GILES_5441,
            FROG_5429
    )

    fun dismissRandom(source: NPC) {
        notify("Random event detected")
        //TODO: pause script thread
        when(source.id) {
            GENIE, GENIE_327 -> {
                source.interact(InteractOption.TALK)
                sleepUntil({ isChatDialogOpen() })
                processDialog()
            }
            else -> {
                source.interact(InteractOption.DISMISS)
            }
        }
    }

}