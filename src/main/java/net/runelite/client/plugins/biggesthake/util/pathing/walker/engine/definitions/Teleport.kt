package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WearableItemTeleport.has
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WearableItemTeleport.teleport
import java.util.*
import java.util.regex.Pattern

enum class Teleport(val moveCost: Int, val location: WorldPoint, val requirement: () -> Boolean, private val action: () -> Boolean) {
    //    VARROCK_TELEPORT(
//            35, new WorldPoint(3212, 3424, 0),
//            () -> Magic.canCast(Spell.Modern.VARROCK_TELEPORT),
//            () -> Magic.cast(Spell.Modern.VARROCK_TELEPORT)
//    ),
//
//    VARROCK_TELEPORT_TAB(
//            35, new WorldPoint(3212, 3424, 0),
//            () -> Inventory.getCount(true, 8007) > 0,
//            () -> Inventory.getFirst(8007).interact("Break")
//    ),
//
//    LUMBRIDGE_TELEPORT(
//            35, new WorldPoint(3225, 3219, 0),
//            () -> Magic.canCast(Spell.Modern.LUMBRIDGE_TELEPORT),
//            () -> Magic.cast(Spell.Modern.LUMBRIDGE_TELEPORT)
//    ),
//
//    LUMBRIDGE_TELEPORT_TAB(
//            35, new WorldPoint(3225, 3219, 0),
//            () -> Inventory.getCount(true, 8008) > 0,
//            () -> Inventory.getFirst(8008).interact("Break")
//    ),
//
//    FALADOR_TELEPORT(
//            35, new WorldPoint(2966, 3379, 0),
//            () -> Magic.canCast(Spell.Modern.FALADOR_TELEPORT),
//            () -> Magic.cast(Spell.Modern.FALADOR_TELEPORT)
//    ),
//
//    FALADOR_TELEPORT_TAB(
//            35, new WorldPoint(2966, 3379, 0),
//            () -> Inventory.getCount(true, 8009) > 0,
//            () -> Inventory.getFirst(8009).interact("Break")
//    ),
//
//    CAMELOT_TELEPORT_TAB(
//            35, new WorldPoint(2757, 3479, 0),
//            () -> Inventory.getCount(true, 8010) > 0,
//            () -> Inventory.getFirst(8010).interact("Break")
//    ),
//
//    PISCATORIS_TELEPORT(
//            35, new WorldPoint(2339, 3649, 0),
//            () -> Inventory.getCount(true, 12408) > 0,
//            () -> Inventory.getFirst(12408).interact("Teleport")
//    ),
//
//    WATSON_TELEPORT(
//            35, new WorldPoint(1645, 3579, 0),
//            () -> Inventory.getCount(true, 23387) > 0,
//            () -> Inventory.getFirst(23387).interact("Teleport")
//    ),
    RING_OF_WEALTH_GRAND_EXCHANGE(
            35, WorldPoint(3161, 3478, 0),
            { has(WearableItemTeleport.RING_OF_WEALTH_MATCHER) },
            { teleport(WearableItemTeleport.RING_OF_WEALTH_MATCHER, Pattern.compile("(?i)Grand Exchange")) }
    ),
    RING_OF_WEALTH_FALADOR(
            35, WorldPoint(2994, 3377, 0),
             { has(WearableItemTeleport.RING_OF_WEALTH_MATCHER) },
             { teleport(WearableItemTeleport.RING_OF_WEALTH_MATCHER, Pattern.compile("(?i)falador.*")) }
    ),
    RING_OF_DUELING_DUEL_ARENA(
            35, WorldPoint(3313, 3233, 0),
             { has(WearableItemTeleport.RING_OF_DUELING_MATCHER) },
             { teleport(WearableItemTeleport.RING_OF_DUELING_MATCHER, Pattern.compile("(?i).*duel arena.*")) }
    ),
    RING_OF_DUELING_CASTLE_WARS(
            35, WorldPoint(2440, 3090, 0),
             { has(WearableItemTeleport.RING_OF_DUELING_MATCHER) },
             { teleport(WearableItemTeleport.RING_OF_DUELING_MATCHER, Pattern.compile("(?i).*Castle Wars.*")) }
    ),
    RING_OF_DUELING_CLAN_WARS(
            35, WorldPoint(3388, 3161, 0),
             { has(WearableItemTeleport.RING_OF_DUELING_MATCHER) },
             { teleport(WearableItemTeleport.RING_OF_DUELING_MATCHER, Pattern.compile("(?i).*Clan Wars.*")) }
    ),
    NECKLACE_OF_PASSAGE_WIZARD_TOWER(
            35, WorldPoint(3113, 3179, 0),
             { has(WearableItemTeleport.NECKLACE_OF_PASSAGE_MATCHER) },
             { teleport(WearableItemTeleport.NECKLACE_OF_PASSAGE_MATCHER, Pattern.compile("(?i).*wizard.+tower.*")) }
    ),
    NECKLACE_OF_PASSAGE_OUTPOST(
            35, WorldPoint(2430, 3347, 0),
             { has(WearableItemTeleport.NECKLACE_OF_PASSAGE_MATCHER) },
             { teleport(WearableItemTeleport.NECKLACE_OF_PASSAGE_MATCHER, Pattern.compile("(?i).*the.+outpost.*")) }
    ),
    NECKLACE_OF_PASSAGE_EYRIE(
            35, WorldPoint(3406, 3156, 0),
             { has(WearableItemTeleport.NECKLACE_OF_PASSAGE_MATCHER) },
             { teleport(WearableItemTeleport.NECKLACE_OF_PASSAGE_MATCHER, Pattern.compile("(?i).*eagl.+eyrie.*")) }
    ),
    COMBAT_BRACE_WARRIORS_GUILD(
            35, WorldPoint(2882, 3550, 0),
             { has(WearableItemTeleport.COMBAT_BRACE_MATCHER) },
             { teleport(WearableItemTeleport.COMBAT_BRACE_MATCHER, Pattern.compile("(?i).*warrior.+guild.*")) }
    ),
    COMBAT_BRACE_CHAMPIONS_GUILD(
            35, WorldPoint(3190, 3366, 0),
             { has(WearableItemTeleport.COMBAT_BRACE_MATCHER) },
             { teleport(WearableItemTeleport.COMBAT_BRACE_MATCHER, Pattern.compile("(?i).*champion.+guild.*")) }
    ),
    COMBAT_BRACE_MONASTRY(
            35, WorldPoint(3053, 3486, 0),
             { has(WearableItemTeleport.COMBAT_BRACE_MATCHER) },
             { teleport(WearableItemTeleport.COMBAT_BRACE_MATCHER, Pattern.compile("(?i).*monastery.*")) }
    ),
    COMBAT_BRACE_RANGE_GUILD(
            35, WorldPoint(2656, 3442, 0),
             { has(WearableItemTeleport.COMBAT_BRACE_MATCHER) },
             { teleport(WearableItemTeleport.COMBAT_BRACE_MATCHER, Pattern.compile("(?i).*rang.+guild.*")) }
    ),
    GAMES_NECK_BURTHORPE(
            35, WorldPoint(2897, 3551, 0),
             { has(WearableItemTeleport.GAMES_NECKLACE_MATCHER) },
             { teleport(WearableItemTeleport.GAMES_NECKLACE_MATCHER, Pattern.compile("(?i).*burthorpe.*")) }
    ),
    GAMES_NECK_BARBARIAN_OUTPOST(
            35, WorldPoint(2520, 3570, 0),
             { has(WearableItemTeleport.GAMES_NECKLACE_MATCHER) },
             { teleport(WearableItemTeleport.GAMES_NECKLACE_MATCHER, Pattern.compile("(?i).*barbarian.*")) }
    ),
    GAMES_NECK_CORPREAL(
            35, WorldPoint(2965, 4832, 2),
             { has(WearableItemTeleport.GAMES_NECKLACE_MATCHER) },
             { teleport(WearableItemTeleport.GAMES_NECKLACE_MATCHER, Pattern.compile("(?i).*corpreal.*")) }
    ),
    GAMES_NECK_WINTER(
            35, WorldPoint(1623, 3937, 0),
             { has(WearableItemTeleport.GAMES_NECKLACE_MATCHER) },
             { teleport(WearableItemTeleport.GAMES_NECKLACE_MATCHER, Pattern.compile("(?i).*wintertodt.*")) }
    ),
    GLORY_EDGE(
            35, WorldPoint(3087, 3496, 0),
             { has(WearableItemTeleport.GLORY_MATCHER) },
             { teleport(WearableItemTeleport.GLORY_MATCHER, Pattern.compile("(?i).*edgeville.*")) }
    ),
    GLORY_KARAMJA(
            35, WorldPoint(2918, 3176, 0),
             { has(WearableItemTeleport.GLORY_MATCHER) },
             { teleport(WearableItemTeleport.GLORY_MATCHER, Pattern.compile("(?i).*karamja.*")) }
    ),
    GLORY_DRAYNOR(
            35, WorldPoint(3105, 3251, 0),
             { has(WearableItemTeleport.GLORY_MATCHER) },
             { teleport(WearableItemTeleport.GLORY_MATCHER, Pattern.compile("(?i).*draynor.*")) }
    ),
    GLORY_AL_KHARID(
            35, WorldPoint(3293, 3163, 0),
             { has(WearableItemTeleport.GLORY_MATCHER) },
             { teleport(WearableItemTeleport.GLORY_MATCHER, Pattern.compile("(?i).*al kharid.*")) }
    );

    fun trigger(): Boolean {
        return action.invoke()
    }

    fun isAtTeleportSpot(position: WorldPoint): Boolean {
        return position.distanceTo2D(location) < 15
    }

    companion object {
        val validStartingWorldPoints: List<WorldPoint>
            get() {
                val positions: MutableList<WorldPoint> = ArrayList()
                for (teleport in values()) {
                    if (!teleport.requirement.invoke()) continue
                    positions.add(teleport.location)
                }
                return positions
            }
    }

}