package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

class BulkPathRequest(val player: PlayerDetails, val requests: List<PathRequestPair>)