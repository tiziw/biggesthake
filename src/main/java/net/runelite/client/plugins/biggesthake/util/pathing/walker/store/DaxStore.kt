package net.runelite.client.plugins.biggesthake.util.pathing.walker.store

import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathLink
import java.util.*

class DaxStore {
    val pathLinks: MutableList<PathLink>
    fun addPathLink(link: PathLink) {
        pathLinks.add(link)
    }

    fun removePathLink(link: PathLink?) {
        pathLinks.remove(link)
    }

    init {
        pathLinks = ArrayList(PathLink.getValues())
    }
}