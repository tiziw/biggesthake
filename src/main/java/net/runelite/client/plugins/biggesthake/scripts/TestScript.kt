package net.runelite.client.plugins.biggesthake.scripts

import net.runelite.client.plugins.biggesthake.LocalScripts
import net.runelite.client.plugins.biggesthake.api.rs.Bank
import net.runelite.client.plugins.biggesthake.api.rs.getNearestNPCs
import net.runelite.client.plugins.biggesthake.api.rs.interact
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.api.script.BaseScript
import net.runelite.client.plugins.biggesthake.enums.InteractOption
import net.runelite.client.plugins.biggesthake.util.pathing.Area
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.RSBank


class TestScript() : BaseScript(LocalScripts.TEST_1) {

    init {
        setParams(area = Area(RSBank.GRAND_EXCHANGE.worldPoint, 40))
    }

    override fun onStart() {
        //logger.info("Player location:  ${player.x} ${player.y}")

        var npc = getNearestNPCs("Banker") // NPC ID ?
        if(npc != null) {
            npc.interact(InteractOption.BANK)
            sleepUntil({ Bank.isOpen() })

            Bank.withdrawItem(itemName = "Vial", amount = 5)
            Bank.close()

            sleep(1500, 2500)
            logger.info("Bank opened? ${Bank.isOpen()}")
            //sleepUntil({!Bank.isOpen()})

            npc.interact(InteractOption.BANK)
            sleepUntil({ Bank.isOpen() })
            Bank.depositItem(itemName = "Vial")
            Bank.close()
        }
    }
}
