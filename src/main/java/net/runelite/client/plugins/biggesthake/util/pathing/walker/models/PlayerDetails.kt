package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

import kotlin.random.Random.Default.nextInt

class PlayerDetails {
    private var attack = 0
    private var defence = 0
    private var strength = 0
    private var hitpoints = 0
    private var ranged = 0
    private var prayer = 0
    private var magic = 0
    private var cooking = 0
    private var woodcutting = 0
    private var fletching = 0
    private var fishing = 0
    private var firemaking = 0
    private var crafting = 0
    private var smithing = 0
    private var mining = 0
    private var herblore = 0
    private var agility = 0
    private var thieving = 0
    private var slayer = 0
    private var farming = 0
    private var runecrafting = 0
    private var hunter = 0
    private var construction = 0
    private var setting: List<IntPair>? = null
    private var varbit: List<IntPair>? = null
    private var member = false
    private var equipment: List<IntPair>? = null
    private var inventory: List<IntPair>? = null

    constructor() {}
    constructor(attack: Int, defence: Int, strength: Int, hitpoints: Int, ranged: Int, prayer: Int, magic: Int, cooking: Int, woodcutting: Int, fletching: Int, fishing: Int, firemaking: Int, crafting: Int, smithing: Int, mining: Int, herblore: Int, agility: Int, thieving: Int, slayer: Int, farming: Int, runecrafting: Int, hunter: Int, construction: Int, setting: List<IntPair>?, varbit: List<IntPair>?, member: Boolean, equipment: List<IntPair>?, inventory: List<IntPair>?) {
        this.attack = attack
        this.defence = defence
        this.strength = strength
        this.hitpoints = hitpoints
        this.ranged = ranged
        this.prayer = prayer
        this.magic = magic
        this.cooking = cooking
        this.woodcutting = woodcutting
        this.fletching = fletching
        this.fishing = fishing
        this.firemaking = firemaking
        this.crafting = crafting
        this.smithing = smithing
        this.mining = mining
        this.herblore = herblore
        this.agility = agility
        this.thieving = thieving
        this.slayer = slayer
        this.farming = farming
        this.runecrafting = runecrafting
        this.hunter = hunter
        this.construction = construction
        this.setting = setting
        this.varbit = varbit
        this.member = member
        this.equipment = equipment
        this.inventory = inventory
    }

    companion object {
        fun generate(): PlayerDetails {
            return PlayerDetails(
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    nextInt(1, 99),
                    null,
                    null,
                    true,
                    null,
                    null
            )
        }
    }
}