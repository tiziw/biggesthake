package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

class IntPair {
    var key = 0
        private set
    var value = 0
        private set

    constructor() {}
    constructor(key: Int, value: Int) {
        this.key = key
        this.value = value
    }

}