package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.Item
import net.runelite.api.ItemComposition
import net.runelite.api.widgets.WidgetInfo
import net.runelite.api.widgets.WidgetItem
import net.runelite.client.plugins.biggesthake.enums.InteractOption
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import java.awt.event.KeyEvent

fun isInventoryEmpty(): Boolean {
    val widget = client.getWidget(WidgetInfo.FIXED_VIEWPORT_INVENTORY_CONTAINER.groupId, WidgetInfo.FIXED_VIEWPORT_INVENTORY_CONTAINER.childId)
    widget?:throw Exception("Unable to access inventory widget")
    return client.getWidget(WidgetInfo.INVENTORY).widgetItems.isEmpty()
}

object Inventory {
    fun isFull(): Boolean {
        return client.getWidget(WidgetInfo.INVENTORY).widgetItems.size == 28
    }

    fun contains(itemId: Int): Boolean {
        return client.getWidget(WidgetInfo.INVENTORY).widgetItems.find { it.id == itemId } != null
    }

    fun contains(name: String): Boolean {
        return client.getWidget(WidgetInfo.INVENTORY).widgetItems.find {it.name().equals(name)} != null
    }

    fun containsOnly(keepItems: Set<String>): Boolean {
        return client.getWidget(WidgetInfo.INVENTORY).widgetItems.all{keepItems.contains(it.name())}
    }

    fun contains(keepItems: Set<String>): Boolean {
        return client.getWidget(WidgetInfo.INVENTORY).widgetItems.any{keepItems.contains(it.name())}
    }
}

fun getInventoryItems(): MutableCollection<WidgetItem> {
    val widget = client.getWidget(WidgetInfo.FIXED_VIEWPORT_INVENTORY_CONTAINER.groupId, WidgetInfo.FIXED_VIEWPORT_INVENTORY_CONTAINER.childId)
    widget?:throw Exception("Unable to access inventory widget")
    return client.getWidget(WidgetInfo.INVENTORY).widgetItems
}

fun dropItem(item: Item) {
    val widget = client.getWidget(WidgetInfo.FIXED_VIEWPORT_INVENTORY_CONTAINER.groupId, WidgetInfo.FIXED_VIEWPORT_INVENTORY_CONTAINER.childId)
    if(widget == null || widget.isSelfHidden) {
        Input.keyPressBlocking(KeyEvent.VK_ESCAPE)
    }
    val inventory = client.getWidget(WidgetInfo.INVENTORY).widgetItems
    var count: Int = 0
    Input.keyPress(KeyEvent.VK_SHIFT)

    Input.setMouseSpeed(1.3)
    for(i in inventory) {
        if(i.id == item.id) {
            val name = client.getItemDefinition(i.id).name
            i.interact(InteractOption.DROP)
            count++
            if(count >= item.quantity) {
                break
            }
        }
    }
    Input.resetMouseSpeed()
    Input.keyRelease(KeyEvent.VK_SHIFT)
}

fun WidgetItem.definition(): ItemComposition {
    return client.getItemDefinition(this.id)
}

fun WidgetItem.name(): String {
    return this.definition().name
}

fun WidgetItem.isNoted(): Boolean {
    return this.definition().isNoted()
}

fun WidgetItem.actions(): Array<out String> {
    return this.definition().inventoryActions
}

fun getEquipmentItems(): MutableCollection<WidgetItem> {
    val widget = client.getWidget(WidgetInfo.FIXED_VIEWPORT_EQUIPMENT_TAB.groupId, WidgetInfo.FIXED_VIEWPORT_EQUIPMENT_TAB.childId)
    widget?:throw Exception("Unable to access inventory widget")
    //return client.getWidget(WidgetInfo.EQUIPMENT).widgetItems
    return ArrayList()
}

fun ItemComposition.isNoted(): Boolean {
    return this.note == 799
}