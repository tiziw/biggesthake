package net.runelite.client.plugins.biggesthake.util.pathing

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.misc.mid
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random

class Area(locA: WorldPoint, locB: WorldPoint) {
    private val maxX = max(locA.x, locB.x)
    private val maxY = max(locA.y, locB.y)

    private val minX = min(locA.x, locB.x)
    private val minY = min(locA.y, locB.y)

    private val plane = locA.plane

    constructor(locA: WorldPoint, radius: Int)
            : this(WorldPoint(locA.x - (radius/2), locA.y - (radius/2)), WorldPoint(locA.x + (radius/2), locA.y + (radius/2)))

    init {
        if(locA.plane != locB.plane) {
            throw Exception("Attempted to define an area spanning planes")
        }
    }

    fun getPosition(): WorldPoint {
        val diffX = maxX - minX
        val diffY = maxY - minY
        return WorldPoint(minX + Random.mid(diffX).toInt(), minY + Random.mid(diffY).toInt(), plane)
    }

    constructor() : this(WorldPoint(Integer.MIN_VALUE, Integer.MIN_VALUE), WorldPoint(Integer.MAX_VALUE, Integer.MAX_VALUE))

    fun contains(loc: WorldPoint): Boolean {
        return loc.x in minX..maxX && loc.y in minY..maxY && loc.plane == plane
    }

    fun center(): WorldPoint {
        val diffX = maxX - minX
        val diffY = maxY - minY
        return WorldPoint(minX + (diffX/2), minY + (diffY/2), plane)
    }

}