package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

enum class Direction(private val matrix: IntArray, val isDiagonal: Boolean) {
    NORTH(intArrayOf(0, 1), false), EAST(intArrayOf(1, 0), false), SOUTH(intArrayOf(0, -1), false), WEST(intArrayOf(-1, 0), false), NORTH_EAST(intArrayOf(1, 1), true), SOUTH_WEST(intArrayOf(-1, -1), true), SOUTH_EAST(intArrayOf(1, -1), true), NORTH_WEST(intArrayOf(-1, 1), true);

    fun translateX(initial: Int): Int {
        return initial + matrix[0]
    }

    fun translateY(initial: Int): Int {
        return initial + matrix[1]
    }

}