package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

import net.runelite.api.Constants.SCENE_SIZE
import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client

class Region constructor(val base: WorldPoint = WorldPoint(client.baseX, client.baseY, client.plane)) {
    val map: Array<Array<Tile?>> = Array(SCENE_SIZE) { arrayOfNulls<Tile>(SCENE_SIZE) }
    operator fun contains(position: WorldPoint): Boolean {
        if (position.plane != base.plane) return false
        if (position.x < base.x) return false
        if (position.x > base.x + 104) return false
        if (position.y < base.y) return false
        return position.y <= base.y + 104
    }
    
    private fun markObject(i: Int, j: Int, tile: net.runelite.api.Tile) {
        if(tile.wallObject != null) {
            val wallConfig = tile.wallObject.config
            val extraFlag = client.getObjectDefinition(tile.wallObject.id).actions.isNotEmpty()
            markWall(i, j, wallConfig.and(31), wallConfig.ushr(6).and(3), extraFlag)
        } else if(tile.gameObjects.isNotEmpty()) {
            this.addClipLocation(i, j,0x100)
        }
    }

    private fun markWall(x: Int, y: Int, orientationA: Int, orientation: Int, objectDefBool: Boolean) {
        if (orientationA == 0) {
            if (0 == orientation) {
                this.addClipLocation(x, y, 128)
                this.addClipLocation(x - 1, y, 8)
            }
            if (1 == orientation) {
                this.addClipLocation(x, y, 2)
                this.addClipLocation(x, y + 1, 32)
            }
            if (2 == orientation) {
                this.addClipLocation(x, y, 8)
                this.addClipLocation(1 + x, y, 128)
            }
            if (3 == orientation) {
                this.addClipLocation(x, y, 32)
                this.addClipLocation(x, y - 1, 2)
            }
        }
        if (orientationA == 1 || 3 == orientationA) {
            if (orientation == 0) {
                this.addClipLocation(x, y, 1)
                this.addClipLocation(x - 1, 1 + y, 16)
            }
            if (orientation == 1) {
                this.addClipLocation(x, y, 4)
                this.addClipLocation(1 + x, 1 + y, 64)
            }
            if (orientation == 2) {
                this.addClipLocation(x, y, 16)
                this.addClipLocation(1 + x, y - 1, 1)
            }
            if (3 == orientation) {
                this.addClipLocation(x, y, 64)
                this.addClipLocation(x - 1, y - 1, 4)
            }
        }
        if (orientationA == 2) {
            if (0 == orientation) {
                this.addClipLocation(x, y, 130)
                this.addClipLocation(x - 1, y, 8)
                this.addClipLocation(x, 1 + y, 32)
            }
            if (1 == orientation) {
                this.addClipLocation(x, y, 10)
                this.addClipLocation(x, y + 1, 32)
                this.addClipLocation(1 + x, y, 128)
            }
            if (orientation == 2) {
                this.addClipLocation(x, y, 40)
                this.addClipLocation(x + 1, y, 128)
                this.addClipLocation(x, y - 1, 2)
            }
            if (orientation == 3) {
                this.addClipLocation(x, y, 160)
                this.addClipLocation(x, y - 1, 2)
                this.addClipLocation(x - 1, y, 8)
            }
        }
        if (objectDefBool) {
            if (orientationA == 0) {
                if (0 == orientation) {
                    this.addClipLocation(x, y, 65536)
                    this.addClipLocation(x - 1, y, 4096)
                }
                if (1 == orientation) {
                    this.addClipLocation(x, y, 1024)
                    this.addClipLocation(x, y + 1, 16384)
                }
                if (2 == orientation) {
                    this.addClipLocation(x, y, 4096)
                    this.addClipLocation(1 + x, y, 65536)
                }
                if (3 == orientation) {
                    this.addClipLocation(x, y, 16384)
                    this.addClipLocation(x, y - 1, 1024)
                }
            }
            if (orientationA == 1 || 3 == orientationA) {
                if (0 == orientation) {
                    this.addClipLocation(x, y, 512)
                    this.addClipLocation(x - 1, 1 + y, 8192)
                }
                if (orientation == 1) {
                    this.addClipLocation(x, y, 2048)
                    this.addClipLocation(1 + x, 1 + y, '\u8000'.toInt())
                }
                if (orientation == 2) {
                    this.addClipLocation(x, y, 8192)
                    this.addClipLocation(1 + x, y - 1, 512)
                }
                if (orientation == 3) {
                    this.addClipLocation(x, y, '\u8000'.toInt())
                    this.addClipLocation(x - 1, y - 1, 2048)
                }
            }
            if (orientationA == 2) {
                if (0 == orientation) {
                    this.addClipLocation(x, y, 66560)
                    this.addClipLocation(x - 1, y, 4096)
                    this.addClipLocation(x, y + 1, 16384)
                }
                if (1 == orientation) {
                    this.addClipLocation(x, y, 5120)
                    this.addClipLocation(x, 1 + y, 16384)
                    this.addClipLocation(x + 1, y, 65536)
                }
                if (orientation == 2) {
                    this.addClipLocation(x, y, 20480)
                    this.addClipLocation(1 + x, y, 65536)
                    this.addClipLocation(x, y - 1, 1024)
                }
                if (orientation == 3) {
                    this.addClipLocation(x, y, 81920)
                    this.addClipLocation(x, y - 1, 1024)
                    this.addClipLocation(x - 1, y, 4096)
                }
            }
        }
    }

    private fun addClipLocation(x: Int, y: Int, i: Int) {
        map[x][y]?.setFlag(i)

    }

    init {
        check(map.size == map[0].size) { "Collision data error" }
        val position = client.localPlayer!!.worldLocation
        for (i in map.indices) {
            for (j in map[i].indices) {
                val tile = client.scene.tiles[client.plane][i][j]
                assert(tile.worldLocation == WorldPoint(i + base.x, j + base.y, base.plane))
                //val blocked = tile.gameObjects.isNotEmpty() || tile.wallObject != null
                //val blockId = if(tile.wallObject != null) tile.wallObject.orientationA else 0x100
                map[i][j] = Tile(tile, i + base.x, j + base.y, base.plane)
            }
        }

        for (i in map.indices) {
            for (j in map[i].indices) {
                markObject(i, j, client.scene.tiles[client.plane][i][j])
                if (map[i][j]!!.x == position.x && map[i][j]!!.y == position.y) map[i][j]!!.isBlocked = false

            }
        }

    }

}