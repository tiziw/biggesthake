package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.Player
import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.api.rs.misc.toMinimap
import net.runelite.client.plugins.biggesthake.api.rs.misc.withinMinimap
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import net.runelite.client.plugins.biggesthake.util.pathing.Area
import net.runelite.client.plugins.biggesthake.util.pathing.walker.DaxWalker
import net.runelite.client.plugins.biggesthake.util.pathing.walker.Server
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.WalkState

val walker = DaxWalker(Server("sub_DPjXXzL5DeSiPf", "PUBLIC-KEY"))

fun walkTo(area: Area) {
    for (i in 0..3) {
        try {
            val walkState = walker.walkTo(area.getPosition(), WalkCondition { area.center().distanceTo(localPlayer.worldLocation) <= 5 })
            if (walkState == WalkState.SUCCESS) break
        } catch (e: Exception) {
            e.printStackTrace();
        }
    }
    //if(walkState != WalkState.SUCCESS)
}

fun walkTo(loc: WorldPoint) {
    walkTo(Area(loc, 5))
}

fun walkToPrecise(loc: WorldPoint) {
    walker.walkTo(loc)
}

fun Player.isAnimating(): Boolean {
    return this.animation != -1
}

fun WorldPoint.walkTo(): Boolean {
    val point = this.toMinimap()
    try {
        point ?: throw Exception("Unable to acquire minimap point, too far? ${!this.withinMinimap()}")
        Input.mouseMove(point.x, point.y)
        Input.leftClick()

        val playerLocation = { client.localPlayer?.worldLocation }
        sleepUntil({ playerLocation() == this || !localPlayer.isMoving() }, 10000, onTimeout = {})
    } catch (e: Exception) {
        e.printStackTrace(); return false
    }

    return localPlayer.worldLocation == this
}

fun WorldPoint(x: Int, y: Int): WorldPoint {
    return WorldPoint(x, y, 0)
}

fun Player.isMoving(): Boolean {
    return client.localDestinationLocation != null
}

// TODO: position randomization
//fun Actor.walkTo() {
//    this.localLocation.walkTo()
//}