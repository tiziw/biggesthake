package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

import net.runelite.api.Player
import net.runelite.client.plugins.biggesthake.util.Helper

object Players {
    val local: Player?
        get() = Helper.client.localPlayer
}