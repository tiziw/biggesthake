package net.runelite.client.plugins.biggesthake.api.rs

import com.google.gson.Gson
import net.runelite.api.World
import net.runelite.api.WorldType
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths


fun isMembersWorld(): Boolean {
    return client.worldType.all { it != WorldType.MEMBERS }
}

fun isMembersWorld(world: World): Boolean {
    return world.types.all { it != WorldType.MEMBERS }
}

fun getFreeWorld(): World? {
    return client.worldList.filter {!isMembersWorld(it) && it.types.size == 0 && it.id != client.world}.random()
}

data class RSAccount(val username: String, val password: String, val members: Boolean) {}

val home = System.getProperty("user.home")
val jsonAccount: String = Files.lines(Paths.get("$home/.config/bhaccount/account.conf"), StandardCharsets.UTF_8).reduce { a, b -> a + "\n" + b }.get();
val defaultAccount = Gson().fromJson(jsonAccount, RSAccount::class.java)