package net.runelite.client.plugins.biggesthake.input

import net.runelite.client.plugins.biggesthake.api.rs.misc.high
import net.runelite.client.plugins.biggesthake.api.rs.misc.low
import kotlin.random.Random

//import java.util.*

class InputConfig {
    companion object {
        private var minDelay = 45
        private val minKeyDelay = 56 + Random.high(0, 8)

         fun getMinDelay(): Long {
             return Random.low(45, 60)
         }

        fun getKeyDelay(): Long {
            return Random.low(minKeyDelay-10, minKeyDelay + 20)
        }

        val wheelDelay: Long
            get() {
                return getMinDelay()
            }
    }
}