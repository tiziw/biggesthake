package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

import net.runelite.api.coords.WorldPoint

class Point3D(val x: Int, val y: Int, val z: Int) {

    fun toWorldPoint(): WorldPoint {
        return WorldPoint(x, y, z)
    }

    companion object {
        fun from(position: WorldPoint?): Point3D {
            return Point3D(position!!.x, position.y, position.plane)
        }
    }

}