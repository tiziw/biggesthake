package net.runelite.client.plugins.biggesthake.util.pathing.walker

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.BulkBankPathRequest
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.BulkPathRequest
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.PathResult
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions.AuthorizationException
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions.RateLimitException
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions.UnknownException
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.io.IOException
import java.util.logging.Level

class Server(key: String, secret: String) {
    private val gson: Gson
    private val key: String
    private val secret: String
    private var rateLimit: Long
    private val okHttpClient: OkHttpClient
    fun getPaths(bulkPathRequest: BulkPathRequest?): List<PathResult> {
        return makePathRequest("$BASE_URL/generatePaths", gson.toJson(bulkPathRequest))
    }

    fun getBankPaths(bulkBankPathRequest: BulkBankPathRequest?): List<PathResult> {
        return makePathRequest("$BASE_URL/generateBankPaths", gson.toJson(bulkBankPathRequest))
    }

    private fun makePathRequest(url: String, jsonPayload: String): List<PathResult> {
        if (System.currentTimeMillis() - rateLimit < 5000L) throw RateLimitException("Throttling requests because key rate limit.")
        val request = generateRequest(url, RequestBody.create(MediaType.parse("application/json"), jsonPayload))
        val start = System.currentTimeMillis()
        try {
            val response = okHttpClient.newCall(request).execute()
            when (response.code()) {
                429 -> {
                    Log.log(Level.WARNING, "Server", "Rate limit hit")
                    rateLimit = System.currentTimeMillis()
                    throw RateLimitException(response.message())
                }
                401 -> throw AuthorizationException(String.format("Invalid API Key [%s]", response.message()))
                200 -> {
                    val responseBody = response.body()
                            ?: throw IllegalStateException("Illegal response returned from server.")
                    Log.info("DaxWalker", String.format("Generated path in %dms", System.currentTimeMillis() - start))
                    return gson.fromJson(responseBody.string(), object : TypeToken<List<PathResult?>?>() {}.type)
                }
            }
        } catch (e: IOException) {
            Log.log(Level.SEVERE, "Server", e.toString())
        }
        throw UnknownException("Error connecting to server.")
    }

    private fun generateRequest(url: String, body: RequestBody): Request {
        return Request.Builder()
                .url(url)
                .post(body)
                .addHeader("key", key)
                .addHeader("secret", secret)
                .addHeader("Content-Type", "application/json")
                .addHeader("User-Agent", "RSPeer: " + "BB's Woodcutter")
                .build()
    }

    companion object {
        private const val BASE_URL = "https://api.dax.cloud/walker"
    }

    init {
        gson = Gson()
        this.key = key
        this.secret = secret
        rateLimit = 0L
        okHttpClient = OkHttpClient()
    }
}