package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions

import net.runelite.api.widgets.Widget

interface RSPopUp {
    fun get(): Widget?
}