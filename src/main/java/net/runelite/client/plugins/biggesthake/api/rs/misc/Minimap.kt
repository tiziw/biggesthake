package net.runelite.client.plugins.biggesthake.api.rs.misc

import net.runelite.api.Perspective
import net.runelite.api.Point
import net.runelite.api.Varbits
import net.runelite.api.coords.LocalPoint
import net.runelite.api.coords.WorldPoint
import net.runelite.api.widgets.Widget
import net.runelite.api.widgets.WidgetInfo
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client

fun WorldPoint.toMinimap(): Point? {
    return LocalPoint.fromWorld(client, this)?.localToMinimap()
}

fun WorldPoint.withinMinimap(): Boolean {
    val localLoc = LocalPoint.fromWorld(client, this)?:throw Exception("Unable to convert $this to local location")
    return localLoc.withinMinimap()
}

fun LocalPoint.withinMinimap(): Boolean {
    val distance = 6100
    val localLocation = client.localPlayer!!.localLocation
    val x = x / 32 - localLocation.x / 32
    val y = y / 32 - localLocation.y / 32

    val dist = x * x + y * y
    return dist < distance
}

fun LocalPoint.localToMinimap(): Point? {
    val distance = 6100
    val localLocation = client.localPlayer!!.localLocation
    val x = x / 32 - localLocation.x / 32
    val y = y / 32 - localLocation.y / 32

    val dist = x * x + y * y
    if (dist < distance) {
        val minimapDrawWidget: Widget?
        minimapDrawWidget = if (client.isResized) {
            if (client.getVar(Varbits.SIDE_PANELS) == 1) {
                client.getWidget(WidgetInfo.RESIZABLE_MINIMAP_DRAW_AREA)
            } else {
                client.getWidget(WidgetInfo.RESIZABLE_MINIMAP_STONES_DRAW_AREA)
            }
        } else {
            client.getWidget(WidgetInfo.FIXED_VIEWPORT_MINIMAP_DRAW_AREA)
        }
        minimapDrawWidget?:return null

        val angle = client.mapAngle and 0x7FF
        val sin = Perspective.SINE[angle]
        val cos = Perspective.COSINE[angle]
        val xx = y * sin + cos * x shr 16
        val yy = sin * x - y * cos shr 16
        val loc = minimapDrawWidget.canvasLocation

        val miniMapX = loc.x + xx + minimapDrawWidget.width / 2
        val miniMapY = minimapDrawWidget.height / 2 + loc.y + yy

        return Point(miniMapX, miniMapY)
    }
    return null
}