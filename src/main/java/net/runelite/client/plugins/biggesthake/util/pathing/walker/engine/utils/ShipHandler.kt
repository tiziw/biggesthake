package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils

import net.runelite.api.GameObject
import net.runelite.client.plugins.biggesthake.api.rs.*
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.enums.EntityType

import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding.BFSMapCache

object ShipHandler {
    private val gangPlankRegex = Regex("(?i)(gang.?plank)")
    private val shipAnchorRegex = Regex("(?i)(ship.s.+|anchor)")
    private val walkAcrossRegex = Regex("(?i)(walk.a)?cross")

    val isOnShip: Boolean
        get() = hasShipObjects() && gangPlank != null && BFSMapCache().accessibleArea < 100

    @JvmStatic
    val offBoat: Boolean
        get() {
            val gangplank = gangPlank ?: return false
            val action = gangplank.actions().find { it.matches(walkAcrossRegex) }
            action?:throw Exception("Unable to find right action on gangplank object: ${gangplank.getName()}")
            return interactor(EntityType.GAMEOBJECT, {gangplank.convexHull.bounds}, action, gangplank.getName()) && sleepUntil({ !isOnShip }, 5000)
        }

    private val gangPlank: GameObject?
        private get() = getNearestObjects().
                find{ sceneObject: GameObject -> sceneObject.distance() < 10 && sceneObject.getName().matches(gangPlankRegex) }

    private fun hasShipObjects(): Boolean {
        return getNearestObjects().filter { sceneObject: GameObject ->
            (sceneObject.distance() < 10
                    && sceneObject.getName().matches(shipAnchorRegex))
        }.isNotEmpty()
    }
}