package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions

import net.runelite.client.plugins.biggesthake.api.rs.*
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.enums.EntityType
import net.runelite.client.plugins.biggesthake.enums.InteractOption

import java.util.regex.Pattern

object WearableItemTeleport {
    @JvmField
    val RING_OF_WEALTH_MATCHER = Pattern.compile("(?i)ring of wealth.?\\(.+")
    @JvmField
    val RING_OF_DUELING_MATCHER = Pattern.compile("(?i)ring of dueling.?\\(.+")
    @JvmField
    val NECKLACE_OF_PASSAGE_MATCHER = Pattern.compile("(?i)necklace of passage.?\\(.+")
    @JvmField
    val COMBAT_BRACE_MATCHER = Pattern.compile("(?i)combat brace.+\\(.+")
    @JvmField
    val GAMES_NECKLACE_MATCHER = Pattern.compile("(?i)game.+neck.+\\(.+")
    @JvmField
    val GLORY_MATCHER = Pattern.compile("(?i).+glory.*\\(.+")
    @JvmStatic
    fun has(itemMatcher: Pattern): Boolean {
        return getInventoryItems().any { itemMatcher.matcher(it.name()).matches() && !it.isNoted() } ||
                getEquipmentItems().any { itemMatcher.matcher(it.name()).matches() }
    }

    @JvmStatic
    fun teleport(itemMatcher: Pattern, option: Pattern): Boolean {
        if(teleportEquipment(itemMatcher, option)) return true

        val item = getInventoryItems().firstOrNull { itemMatcher.matcher(it.name()).matches() && !it.isNoted() }
        item?: return false

        val action = item.actions().firstOrNull { option.matcher(it).matches() }
        if (action != null && !interactor(EntityType.WIDGET, {item.canvasBounds}, action, item.name()) && !item.interact(InteractOption.RUB)) return false;
        if (!sleepUntil({ isChatDialogOpen() }, 5000)) return false;

        return processOption(condition = { option.matcher(it).matches() });
    }

    fun teleportEquipment(itemMatcher: Pattern, option: Pattern): Boolean {
        val item = getEquipmentItems().firstOrNull { itemMatcher.matcher(it.name()).matches() }?: return false
        val action = item.actions().firstOrNull {  option.matcher(it).matches() }?: return false
        return item.interact(action = action)
    }
}