package net.runelite.client.plugins.biggesthake.enums

import net.runelite.client.plugins.biggesthake.api.rs.Bank
import net.runelite.client.plugins.biggesthake.api.rs.isChatDialogOpen

enum class InteractOption(val asString: String, val timeoutTask: (() -> Boolean)?) {
    TALK("Talk-to", { isChatDialogOpen() }), // Need to make sure it works for all kind of dialogs
    BANK("Bank", { Bank.isOpen()}),
    EXCHANGE("Exchange"),
    ATTACK("Attack"),
    FOLLOW("Follow"),
    USE("Use"),
    EAT("Eat"),
    DROP("Drop"),
    DISMISS("Dismiss"),
    RUB("Rub"),
    NONE(""),
    CHOP_DOWN("Chop down");

    constructor(asString: String) : this(asString, null)
}