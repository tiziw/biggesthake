package net.runelite.client.plugins.biggesthake.api.rs

import net.runelite.api.GameObject
import net.runelite.api.NPC
import net.runelite.api.ObjectComposition
import net.runelite.api.Player
import net.runelite.client.plugins.biggesthake.core.EventManager
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client

val localPlayer: Player
    get() {
        return client.localPlayer!!
    }

fun getNearestNPCs(): List<NPC> {
    return client.npcs.sortedBy { it.worldLocation.distanceTo2D(client.localPlayer?.worldLocation) }
}

fun getNearestNPCs(name: String): NPC? {
    return getNearestNPCs().find { it.name?.toLowerCase().equals(name.toLowerCase()) }
}

fun getGameObjects(): List<GameObject> {
//    val tiles = client.scene.tiles[client.plane]
//    val objects = ArrayList<GameObject?>()
//    for (i in 0 until Constants.SCENE_SIZE) {
//        for (j in 0 until Constants.SCENE_SIZE) {
//            objects.addAll(tiles[i][j].gameObjects)
//        }
//    }
    return EventManager.gameObjects
}

fun getNearestObjects(): List<GameObject> {
    return getGameObjects().sortedBy { it.worldLocation.distanceTo2D(client.localPlayer?.worldLocation) }
}

fun getGameObject(id: Int): GameObject? {
    val list = getGameObjects().sortedBy { it.worldLocation.distanceTo2D(client.localPlayer!!.worldLocation) }
    return list.find { it.getComposition().id == id}
}

fun getObject(name: String): GameObject? {
    return getNearestObjects().find {it.getName() == name}
}

fun GameObject.getComposition(): ObjectComposition {
    val objectComposition = client.getObjectDefinition(id)
    return if (objectComposition == null || objectComposition.impostorIds == null) objectComposition else objectComposition.impostor
}

fun GameObject.getName(): String {
    return client.getObjectDefinition(this.id).name
}

fun GameObject.distance(): Int {
    return this.worldLocation.distanceTo2D(client.localPlayer?.worldLocation)
}

fun GameObject.actions(): List<String> {
    return getComposition().actions.filterNotNull()
}