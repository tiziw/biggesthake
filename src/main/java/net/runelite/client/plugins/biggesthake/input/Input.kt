package net.runelite.client.plugins.biggesthake.input

import net.runelite.client.plugins.biggesthake.api.rs.misc.addClickPoint
import net.runelite.client.plugins.biggesthake.api.rs.misc.mid
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.input.GuiHelper.Companion.getCanvasOffset
import net.runelite.client.plugins.biggesthake.input.MouseUtil.setMouseFactor
import net.runelite.client.plugins.biggesthake.util.Helper
import java.awt.Robot
import java.awt.event.InputEvent
import java.awt.event.KeyEvent
import java.lang.Thread.sleep
import kotlin.math.abs
import kotlin.random.Random

object Input : Robot() {
    private val motionFactory = MouseUtil.getCustomFactory()
    private var lastRelease: Long = 0
    private var currentFactor: Double = 1.0

    fun resetMouseSpeed() {
        currentFactor = 1.0
        setMouseFactor(1.0)
    }

    fun setMouseSpeed(d: Double) {
        currentFactor = d
        setMouseFactor(d)
    }

    @Synchronized
    override fun mouseMove(x: Int, y: Int) {
        setMouseFactor(currentFactor + (Random.mid(2,5).toDouble()/100.0))
        var xOld = 0
        var yOld = 0
        var xLast = 0
        var yLast = 0
        motionFactory.build(x, y, Helper.frame, getCanvasOffset()).move { x1: Int, y1: Int ->
            xLast = xOld
            yLast = yOld
            xOld = x1
            yOld = y1
            super.mouseMove(x1, y1)
        }
        addClickPoint(GuiHelper.mouseX, GuiHelper.mouseY)
//        var angle = Point(x-xLast, y-yLast)
        //resetMouseSpeed()
    }

    @Synchronized
    fun mousePressAndRelease(buttonID: Int) {
        val timeDiff = System.currentTimeMillis() - lastRelease
        if(timeDiff <= 275) sleep(Random.mid(abs(275-timeDiff), 100))
        mousePress(buttonID)
        mouseRelease(buttonID)
    }

    fun leftClick() {
        mousePressAndRelease(1).sleep()
    }

    fun rightClick() {
        mousePressAndRelease(3).sleep()
    }

    @Synchronized
    override fun mouseRelease(buttonID: Int) {
        super.mouseRelease(InputEvent.getMaskForButton(buttonID))
        lastRelease = System.currentTimeMillis()
        sleep()
    }

    @Synchronized
    override fun mousePress(buttonID: Int) {
        super.mousePress(InputEvent.getMaskForButton(buttonID))
        sleep(Random.mid(50, 90))
    }

    //TODO: Symbols are nut supported at this time
    @Synchronized
    fun typeMessage(message: String) {
        val charArray = message.toCharArray().forEach {
            keyPressBlocking(it)
            sleep(Random.mid(0, 19))
        }
        keyPress(KeyEvent.VK_ENTER)
    }

    @Synchronized
    override fun keyPress(keycode: Int) {
        super.keyPress(keycode).sleep()
    }

    @Synchronized
    fun keyPressBlocking(keyCode: Int) {
        super.keyPress(keyCode)
        sleep(InputConfig.getKeyDelay())
        super.keyRelease(keyCode)
        sleep(InputConfig.getKeyDelay())
    }

    fun keyPressBlocking(char: Char) {
        val keyCode = KeyEvent.getExtendedKeyCodeForChar(char.toInt());
        keyPressBlocking(keyCode)
    }
    
    @Synchronized
    override fun keyRelease(keycode: Int) {
        super.keyRelease(keycode).sleep()
    }

//    @Synchronized
//    fun holdKey(keycode: Int, timeMS: Int) {
//        Thread() {
//            peer.keyPress(keycode)
//            sleepRand()
//            peer.keyRelease(keycode)
//            Unit.sleep()
//        }.start()
//    }

}
