package net.runelite.client.plugins.biggesthake.api.script

import net.runelite.client.plugins.biggesthake.LocalScripts
import net.runelite.client.plugins.biggesthake.api.rs.Bank
import net.runelite.client.plugins.biggesthake.api.rs.Inventory
import net.runelite.client.plugins.biggesthake.api.rs.localPlayer
import net.runelite.client.plugins.biggesthake.api.rs.walkTo
import net.runelite.client.plugins.biggesthake.core.ScriptManager.Companion.currentScript
import net.runelite.client.plugins.biggesthake.util.pathing.Area
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.RSBank
import kotlin.random.Random

data class ScriptParam(val scriptType: LocalScripts) {
    var preferredBank: RSBank? = null
    var region: Area = Area()
    var autoBank: Boolean = false
    var keepItems: Set<String> = hashSetOf()

    val scriptState = StateObject({ false }, {}, "${scriptType.name} state")

    init {
        ScriptState.values().forEach {it.dependency?:scriptState.addChild(it.state) }
    }

    val isInRegion = { !region.contains(localPlayer.worldLocation) }
    val walkToRegion = { walkTo(region) }

    val shouldBank = { autoBank && ((Inventory.isFull() && !Inventory.containsOnly(keepItems)) || !Inventory.contains(keepItems))}
    val doBanking = {
        var bank = preferredBank
        if(bank == null) bank = RSBank.getClosestBank(localPlayer.worldLocation)
        bank?:throw Exception("Unable to find bank")

        walkTo(bank.worldPoint)
        Bank.open()
        Bank.depositItems {!keepItems.contains(it)}
        // Make sure inventory contains the keep items
        keepItems.forEach { if(!Inventory.contains(it)) Bank.withdrawItem(itemName = it, amount = 1) }
        if(Random.nextBoolean())
        Bank.close()
    }

    enum class ScriptState(val state: StateObject, val dependency: StateObject?) {
        WALK_TO_REGION({scriptParam.isInRegion()}, {scriptParam.walkToRegion()}, "Walking to region"),
        DO_BANK({scriptParam.shouldBank()}, {scriptParam.doBanking()}, "Walking to bank", WALK_TO_REGION),
        ;

        constructor(stateVerifier: () -> Boolean, stateExecutor: () -> Unit, name: String, parent: ScriptState? = null)
            : this(StateObject(stateVerifier, stateExecutor, name, isEnum = true, parent = parent?.state), parent?.state)
    }

    companion object {
        val scriptParam: ScriptParam
            get() = currentScript?.params!!
    }

}
