package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

class PathRequestPair(val start: Point3D, val end: Point3D)