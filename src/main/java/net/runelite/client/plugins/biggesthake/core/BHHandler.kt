package net.runelite.client.plugins.biggesthake.core

import net.runelite.api.Client
import net.runelite.client.Notifier
import net.runelite.client.callback.ClientThread
import net.runelite.client.plugins.biggesthake.BHConfiguration
import net.runelite.client.plugins.biggesthake.api.rs.misc.startResetSeedTimer
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.ui.ClientUI

class BHHandler(client: Client, clientUI: ClientUI, private val config: BHConfiguration, val clientThread: ClientThread, val notifier: Notifier) {
    private var gui: ScriptSelector?

    init {
        Helper.init(client, clientUI, clientThread, notifier)
        ScriptManager.init(config)
        // Resets random value
        startResetSeedTimer()
        // Start/stop script Interface
        gui = ScriptSelector(this)
    }

    fun shutDown() {
        gui?.shutDown()
        ScriptManager.stop()
    }
}
