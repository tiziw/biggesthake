package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions

import net.runelite.api.coords.WorldPoint

interface PathLinkHandler {
    fun handle(start: WorldPoint?, end: WorldPoint?, walkCondition: WalkCondition?): PathHandleState?
}