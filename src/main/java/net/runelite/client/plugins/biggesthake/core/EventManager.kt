package net.runelite.client.plugins.biggesthake.core

import net.runelite.api.GameObject
import net.runelite.api.GameState
import net.runelite.api.NPC
import net.runelite.api.events.*
import net.runelite.client.eventbus.Subscribe
import net.runelite.client.plugins.biggesthake.core.ScriptManager.Companion.currentScript
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import net.runelite.client.plugins.biggesthake.util.RandomHandler

object EventManager {
    @get:Synchronized
    val gameObjects = ArrayList<GameObject>()

    @Subscribe
    fun onInteractingChanged(event: InteractingChanged) {
        val source = event.source
        val target = event.target
        val player = client.localPlayer
        if (player == null || target !== player || player.interacting === source || source !is NPC
                || !RandomHandler.EVENT_NPCS.contains(source.id)) {
            return
        }
        RandomHandler.dismissRandom(source)
    }

    @Subscribe
    fun onGameObjectSpawned(event: GameObjectSpawned) {
        gameObjects.add(event.gameObject)
    }

    @Subscribe
    fun onGameObjectDespawned(event: GameObjectDespawned) {
        gameObjects.remove(event.gameObject)
    }

    @Subscribe
    fun onGameObjectChanged(event: GameObjectChanged) {
        gameObjects.remove(event.gameObject)
    }

    @Subscribe
    fun onAnimationChanged(event: AnimationChanged) {
        currentScript?.register(event)
    }

    @Subscribe
    fun onGameStateChanged(event: GameStateChanged) {
        if (event.gameState != GameState.LOGGED_IN) {
            // Logged out?
        }
    }

}