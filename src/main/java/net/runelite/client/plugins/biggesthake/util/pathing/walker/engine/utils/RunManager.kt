package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils

import net.runelite.client.plugins.biggesthake.api.rs.isMoving
import net.runelite.client.plugins.biggesthake.api.rs.localPlayer

class RunManager {
    private val initial: Long = System.currentTimeMillis()
    val isWalking: Boolean
        get() = System.currentTimeMillis() - initial < 1300 || localPlayer.isMoving()

}