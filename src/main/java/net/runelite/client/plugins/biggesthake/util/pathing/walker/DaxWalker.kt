package net.runelite.client.plugins.biggesthake.util.pathing.walker

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.WalkerEngine
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.Teleport
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.*
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions.AuthorizationException
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions.RateLimitException
import net.runelite.client.plugins.biggesthake.util.pathing.walker.models.exceptions.UnknownException
import net.runelite.client.plugins.biggesthake.util.pathing.walker.store.DaxStore
import java.util.*
import java.util.stream.Collectors

class DaxWalker(private val server: Server) {
    private val walkerEngine: WalkerEngine = WalkerEngine(null, this)
    val store: DaxStore = DaxStore()
    private var isUseTeleports: Boolean = false

    /**
     * This condition will override the default walk condition that enables run for you.
     *
     * @param walkCondition
     */
    fun setGlobalCondition(walkCondition: WalkCondition?) {
        walkerEngine.setWalkCondition(walkCondition)
    }

    /**
     *
     * @param positionable
     * @param walkCondition Will trigger WITH with the global condition.
     * @return
     */
    @JvmOverloads
    fun walkTo(positionable: WorldPoint?, walkCondition: WalkCondition? = null): WalkState {
        Log.info("DaxWalker", String.format("Navigating to (%d,%d,%d)", positionable!!.x, positionable.y, positionable.plane))
        val pathRequestPairs = if (isUseTeleports) getPathTeleports(positionable) else ArrayList()
        pathRequestPairs.add(PathRequestPair(Point3D.from(localWorldPoint()), Point3D.from(positionable)))
        val request = BulkPathRequest(PlayerDetails.generate(), pathRequestPairs)
        return try {
            if (walkerEngine.walk(server.getPaths(request), walkCondition)) WalkState.SUCCESS else WalkState.FAILED
        } catch (e: RateLimitException) {
            WalkState.RATE_LIMIT
        } catch (e: AuthorizationException) {
            WalkState.ERROR
        } catch (e: UnknownException) {
            WalkState.ERROR
        }
    }

    fun walkToBank(walkCondition: WalkCondition?): WalkState {
        return walkToBank(null, walkCondition)
    }

    /**
     *
     * @param bank
     * @param walkCondition Will trigger WITH the global condition.
     * @return
     */
    @JvmOverloads
    fun walkToBank(bank: RSBank? = null, walkCondition: WalkCondition? = null): WalkState {
        if (bank != null) return walkTo(bank.worldPoint)
        val pathRequestPairs = if (isUseTeleports) bankPathTeleports else ArrayList()
        pathRequestPairs.add(BankPathRequestPair(Point3D.from(localWorldPoint()), null))
        val request = BulkBankPathRequest(PlayerDetails.generate(), pathRequestPairs)
        return try {
            if (walkerEngine.walk(server.getBankPaths(request), walkCondition)) WalkState.SUCCESS else WalkState.FAILED
        } catch (e: RateLimitException) {
            WalkState.RATE_LIMIT
        } catch (e: AuthorizationException) {
            WalkState.ERROR
        } catch (e: UnknownException) {
            WalkState.ERROR
        }
    }

    private fun localWorldPoint(): WorldPoint {
        return Helper.client.localPlayer!!.worldLocation
    }

    private val bankPathTeleports: MutableList<BankPathRequestPair>
        private get() = Teleport.validStartingWorldPoints.stream()
                .map { position: WorldPoint? -> BankPathRequestPair(Point3D.Companion.from(position), null) }
                .collect(Collectors.toList())

    private fun getPathTeleports(start: WorldPoint?): MutableList<PathRequestPair> {
        return Teleport.validStartingWorldPoints.stream()
                .map { position: WorldPoint? -> PathRequestPair(Point3D.Companion.from(position), Point3D.Companion.from(start)) }
                .collect(Collectors.toList())
    }

}