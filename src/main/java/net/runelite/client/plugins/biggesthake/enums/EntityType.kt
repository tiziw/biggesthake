package net.runelite.client.plugins.biggesthake.enums

import net.runelite.client.plugins.biggesthake.api.rs.misc.low
import net.runelite.client.plugins.biggesthake.api.rs.misc.mid
import java.awt.Point
import kotlin.random.Random

val defaultRandomizer: (Int, Int) -> Point = {
    w, h -> Point(Random.mid(w).toInt(), Random.mid(h).toInt())
}

// TODO: Make randomizer choose closest location based on current mouse location
// Prolly not needed since jagex doesn't know bounds when clicking,
// could be useful for realistic randomness in some cases

enum class EntityType(val randomConsumer: (Int, Int) -> Point, val distanceDependant: Boolean) {
    WIDGET({w,h -> widgetItemClickPoint(w, h) }, false),
    GAMEOBJECT(defaultRandomizer, true),
    // NPC click more often to the head, low because y axis is reversed
    NPC({w,h -> npcClickPoint(w, h) }, true),
    PLAYER(defaultRandomizer, true),
    GROUND_OBJECT(defaultRandomizer, true),
    GENERIC(defaultRandomizer, true);
}

fun widgetItemClickPoint(width: Int, height: Int): Point {
    if(Random.nextInt(0, 10) == 1) {
        return Point(Random.mid(width).toInt(), Random.mid(height).toInt())
    }
    val centerW = width / 2
    val centerH = height / 2

    val hW = centerW / 2
    val hH = centerH / 2
    
    return Point(Random.mid(centerW-hW, centerW+hW).toInt(), Random.mid(centerH-hH, centerH+hH).toInt())
}

fun npcClickPoint(width: Int, height: Int): Point {
    if(Random.nextInt(0, 10) == 1) {
        return Point(Random.nextInt(0, width), Random.nextInt(0, height))
    } else if(Random.nextInt(0, 4) == 0) {
        return Point(Random.mid(width).toInt(), Random.low(height).toInt())
    }
    val centerW = width / 2
    val centerH = height / 2

    val hW = centerW / 2
    val hH = centerH / 2

    return Point(Random.mid(centerW-hW, centerW+hW).toInt(), Random.low(hH/4, centerH+hH).toInt())
}