package net.runelite.client.plugins.biggesthake.util.pathing.walker.models

class PathResult {
    val pathStatus: PathStatus? = null
    val path: List<Point3D?>? = null
    val cost = 0

}