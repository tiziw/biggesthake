package net.runelite.client.plugins.biggesthake.util;

import net.runelite.client.ui.overlay.*;

import java.awt.*;
import java.util.List;

public class OverlayProxy extends Overlay {

    public OverlayProxy() {
        super();
        setPosition(OverlayPosition.DYNAMIC);
        setLayer(OverlayLayer.ALWAYS_ON_TOP);
        setPriority(OverlayPriority.HIGHEST);
    }

    @Override
    public Dimension render(Graphics2D graphics2D) {
        return null;
    }

    @Override
    public void setPreferredLocation(Point preferredLocation) {
        super.setPreferredLocation(preferredLocation);
    }

    @Override
    public Rectangle getBounds() {
        return super.getBounds();
    }

    @Override
    public Point getPreferredLocation() {
        return super.getPreferredLocation();
    }

    @Override
    public Dimension getPreferredSize() {
        return super.getPreferredSize();
    }

    @Override
    public OverlayPosition getPreferredPosition() {
        return super.getPreferredPosition();
    }

    @Override
    public OverlayPosition getPosition() {
        return super.getPosition();
    }

    @Override
    public OverlayPriority getPriority() {
        return super.getPriority();
    }

    @Override
    public OverlayLayer getLayer() {
        return super.getLayer();
    }

    @Override
    public List<OverlayMenuEntry> getMenuEntries() {
        return super.getMenuEntries();
    }


    @Override
    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
    }

    @Override
    public void setPreferredPosition(OverlayPosition preferredPosition) {
        super.setPreferredPosition(preferredPosition);
    }

    @Override
    public void setBounds(Rectangle bounds) {
        super.setBounds(bounds);
    }

    @Override
    public void setPosition(OverlayPosition position) {
        super.setPosition(position);
    }

    @Override
    public void setPriority(OverlayPriority priority) {
        super.setPriority(priority);
    }

    @Override
    public void setLayer(OverlayLayer layer) {
        super.setLayer(layer);
    }

}
