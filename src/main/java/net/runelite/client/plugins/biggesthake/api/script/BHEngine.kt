package net.runelite.client.plugins.biggesthake.api.script

import net.runelite.api.GameState.*
import net.runelite.api.widgets.WidgetInfo
import net.runelite.client.plugins.biggesthake.api.rs.*
import net.runelite.client.plugins.biggesthake.api.rs.misc.getLogger
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleep
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil
import net.runelite.client.plugins.biggesthake.input.Input
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import java.awt.event.KeyEvent

class BHEngine {
    companion object {

        fun handleLogin() {
            getLogger().info("Currently in state ${client.gameState.name}")

            val account = defaultAccount
            when(client.gameState) {
                LOGIN_SCREEN -> {
                    if(!isMembersWorld() && !defaultAccount.members) {
                        client.changeWorld(getFreeWorld())
                    }

                    while(client.loginIndex == 0) {
                        Input.keyPressBlocking(KeyEvent.VK_ENTER)
                        sleepUntil({client.loginIndex != 0}, 1000, onTimeout = {})
                    }

                    client.username = account.username
                    if(client.currentLoginField != 1) {
                        Input.keyPressBlocking(KeyEvent.VK_ENTER)
                    }
                    Input.typeMessage(account.password)
                    Input.keyPressBlocking(KeyEvent.VK_ENTER)
                    sleepUntil({client.gameState == LOGGING_IN }, 10000, onTimeout= {})
                    handleLogin()
                }
                LOGGING_IN -> {
                    sleepUntil({client.gameState == LOGGED_IN }, 10000, onTimeout = { })
                    sleep(800, 1800)
                    handleLogin()
                }
                LOGGED_IN -> {
                    if(isWidgetOpen(WidgetInfo.LOGIN_CLICK_TO_PLAY_SCREEN)) {
                        val playButton = client.getWidget(WidgetInfo.LOGIN_CLICK_TO_PLAY_SCREEN.groupId, 81)
                        playButton.click()
                        sleepUntil({!isWidgetOpen(WidgetInfo.LOGIN_CLICK_TO_PLAY_SCREEN)}, 10000, onTimeout = { handleLogin()})
                        sleep(1800, 2200)
                    }
                }
                HOPPING -> {
                    sleepUntil({ client.gameState != HOPPING}, 10000, onTimeout = { handleLogin()})
                }
                CONNECTION_LOST -> {
                    sleepUntil( {client.gameState != CONNECTION_LOST}, 10000, onTimeout = { })
                    sleep(800, 1800)
                    handleLogin()
                }
            }

        }
    }
}