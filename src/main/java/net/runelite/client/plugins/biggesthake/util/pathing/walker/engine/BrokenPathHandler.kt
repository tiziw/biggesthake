package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine

import net.runelite.api.GameObject
import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.*
import net.runelite.client.plugins.biggesthake.api.rs.misc.mid
import net.runelite.client.plugins.biggesthake.api.rs.misc.sleepUntil

import net.runelite.client.plugins.biggesthake.util.pathing.walker.Log
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathHandleState
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PathLink
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.PopUpInterfaces
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.definitions.WalkCondition
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding.BFSMapCache
import net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils.RunManager
import java.util.concurrent.atomic.AtomicBoolean
import java.util.function.Predicate
import java.util.logging.Level
import java.util.regex.Pattern
import kotlin.math.abs
import kotlin.random.Random

object BrokenPathHandler {
    private fun generateCase(predicate: Predicate<GameObject>, pattern: Pattern): DestinationStateSpecialCase {
        return object : DestinationStateSpecialCase {
            override fun satisfies(sceneObject: GameObject): Boolean {
                return predicate.test(sceneObject)
            }

            override val pattern: Pattern
                get() = pattern
        }
    }

    @JvmStatic
    fun handle(start: WorldPoint, end: WorldPoint, walkCondition: WalkCondition): PathHandleState {
        val sceneObject = getBlockingObject(start, end)
        if (sceneObject != null) return handleObject(start, end, sceneObject, walkCondition)
        Log.severe(String.format(
                "No PathLink handler for (%d, %d, %d)->(%d, %d, %d)",
                start.x, start.y, start.plane,
                end.x, end.y, end.plane
        ))
        return PathHandleState.FAILED
    }

    @JvmStatic
    fun handlePathLink(start: WorldPoint, end: WorldPoint, walkCondition: WalkCondition?, pathLinks: List<PathLink?>): PathHandleState? {
        val pathLink = pathLinks.stream()
                .filter { link: PathLink? -> link!!.start == start && link.end == end }
                .findAny()
                .orElse(null)
        return pathLink?.handle(walkCondition)
    }

    private val gateRegex = Regex("(?i)Gate of (.+)")

    private fun handleObject(start: WorldPoint, end: WorldPoint, sceneObject: GameObject, walkCondition: WalkCondition): PathHandleState {
        if (sceneObject.getName().matches(gateRegex))
            return handleStrongHoldDoor(start, end, sceneObject, walkCondition);

        Log.log(Level.FINE, "DaxWalker", "Handling " + sceneObject.getName() + "->" + sceneObject.actions());
        if (!determine(start, end).handle(sceneObject)) return PathHandleState.FAILED;

        val runManager = RunManager();
        val exitCondition = AtomicBoolean(false);
        if (!sleepUntil({
            if (!runManager.isWalking) {
                sleepUntil({ PopUpInterfaces.resolve() || end.distanceTo2D(localPlayer.worldLocation) <= 2 || BFSMapCache().canReach(end) }, if(localPlayer.isAnimating()) Random.mid(1200, 2000) else Random.mid(3500, 4500) );
                true;
            }
            if (walkCondition.asBoolean) {
                exitCondition.set(true);
                true;
            }
            false
        }, 15000)) {
            return PathHandleState.FAILED;
        }
        PopUpInterfaces.resolve();
        if (exitCondition.get()) return PathHandleState.EXIT;
        if (isChatDialogOpen())
            return if(EntityHandler.handleConversation()) PathHandleState.SUCCESS else PathHandleState.FAILED
        
        return PathHandleState.SUCCESS
    }

    private fun handleStrongHoldDoor(start: WorldPoint, end: WorldPoint, sceneObject: GameObject, walkCondition: WalkCondition): PathHandleState {
        if (!determine(start, end).handle(sceneObject)) return PathHandleState.FAILED
        val runManager = RunManager()
        val exitCondition = AtomicBoolean(false)
        if (!sleepUntil({
                    if (!runManager.isWalking) {
                        return@sleepUntil true
                    }
                    if (walkCondition.asBoolean) {
                        exitCondition.set(true)
                        return@sleepUntil true
                    }
                    false
                }, 15000)) {
            return PathHandleState.FAILED
        }
        if (exitCondition.get()) return PathHandleState.EXIT
        if (!isChatDialogOpen()) return PathHandleState.FAILED
        return if (EntityHandler.handleConversation()) PathHandleState.SUCCESS else PathHandleState.FAILED
    }

    private fun getBlockingObject(start: WorldPoint, end: WorldPoint): GameObject? {
        val state = determine(start, end)

        return getNearestObjects().firstOrNull() { it.worldLocation.distanceTo2D(start) <= 5 && state.objectSatisfies(it) }
    }

    private fun containsAction(interactable: GameObject, regex: Pattern): Boolean { 
        return interactable.actions().find {regex.matcher(it).matches()} != null
    }

    private fun determine(start: WorldPoint, end: WorldPoint): NextMove {
        if (start.plane < end.plane || isStrongHoldUp(start, end)) return NextMove.FLOOR_ABOVE
        if (isStrongHold(start) && end.y < 3500) return NextMove.FLOOR_ABOVE
        if (start.y > 5000 && end.y < 5000) return NextMove.FLOOR_ABOVE
        if (start.y < 5000 && end.y > 5000) return NextMove.UNDERGROUND
        return if (start.plane > end.plane || isStrongHoldDown(start, end)) NextMove.FLOOR_UNDER else NextMove.SAME_FLOOR
    }

    private fun isStrongHold(position: WorldPoint): Boolean {
        return position.x in 1850..2380 && position.y >= 5175 && position.y <= 5317
    }

    private fun isStrongHoldUp(start: WorldPoint, end: WorldPoint): Boolean {
        return isStrongHold(start) && isStrongHold(end) && abs(end.x - start.x) > 52 && start.x > end.x
    }

    private fun isStrongHoldDown(start: WorldPoint, end: WorldPoint): Boolean {
        return isStrongHold(start) && isStrongHold(end) && abs(end.x - start.x) > 52 && start.x < end.x
    }

    private val trapDoorRegex = Regex("(?i)(trap.?door|manhole)")
    private val chestRegex = Regex("chest.*")

    enum class NextMove(private val pattern: Pattern, vararg specialCases: DestinationStateSpecialCase) {
        UNDERGROUND(Pattern.compile("(?i)(climb|jump|walk).down"),
                generateCase(Predicate<GameObject> { sceneObject: GameObject -> sceneObject.getName().matches(trapDoorRegex) }, Pattern.compile("(?i)Open|(Climb.down)"))
        ),
        FLOOR_UNDER(Pattern.compile("(?i)(climb|jump|walk).down"),
                generateCase(Predicate<GameObject> { sceneObject: GameObject -> sceneObject.getName().matches(trapDoorRegex) }, Pattern.compile("(?i)Open|(Climb.down)"))
        ),
        FLOOR_ABOVE(Pattern.compile("(?i)(pass|climb|jump|walk).(up|through)")), SAME_FLOOR(Pattern.compile("(Exit|Use(?i-m)|pass|(walk|jump|climb).(across|over|under|into)|(open|push|enter)|(.+.through)|cross|board|mine)"));

        private val specialCases: Array<DestinationStateSpecialCase>
        fun objectSatisfies(sceneObject: GameObject): Boolean {
            for (specialCase in specialCases) {
                if (specialCase.satisfies(sceneObject)) return true
            }
            return if (sceneObject.getName().matches(chestRegex)) false else containsAction(sceneObject, pattern)
        }

        fun handle(sceneObject: GameObject): Boolean {
            for (specialCase in specialCases) {
                if (specialCase.satisfies(sceneObject)) return specialCase.handle(sceneObject)
            }
            return sceneObject.interact { s: String? -> pattern.matcher(s).matches() }
        }

        fun handle(): Boolean {
            val sceneObject: GameObject? = getNearestObjects().first(this::objectSatisfies)
            return sceneObject != null && handle(sceneObject)
        }

        init {
            this.specialCases = specialCases as Array<DestinationStateSpecialCase>
        }
    }

    private interface DestinationStateSpecialCase {
        fun satisfies(sceneObject: GameObject): Boolean
        val pattern: Pattern
        fun handle(sceneObject: GameObject): Boolean {
            return sceneObject.interact { s: String? -> pattern.matcher(s).matches() }
        }
    }
}