package net.runelite.client.plugins.biggesthake.input

import com.github.joonasvali.naturalmouse.api.MouseMotion
import com.github.joonasvali.naturalmouse.api.MouseMotionFactory
import com.github.joonasvali.naturalmouse.support.*
import com.github.joonasvali.naturalmouse.util.FlowTemplates
import net.runelite.api.Constants
import net.runelite.client.plugins.biggesthake.api.rs.misc.low
import net.runelite.client.plugins.biggesthake.api.rs.misc.mid
import net.runelite.client.plugins.biggesthake.api.rs.misc.random
import net.runelite.client.plugins.biggesthake.enums.defaultRandomizer
import net.runelite.client.plugins.biggesthake.input.GuiHelper.Companion.stretchedDim
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.ui.ContainableFrame
import org.slf4j.LoggerFactory
import java.awt.Point
import java.awt.Rectangle
import java.util.*
import kotlin.random.Random

object MouseUtil {
    val logger = LoggerFactory.getLogger(MouseUtil::class.java)
    val mouseBaseTime: Long = Random.low(300, 315)
    var manager: DefaultSpeedManager = DefaultSpeedManager()

    fun setMouseFactor(factor: Double) {
        manager.setMouseMovementBaseTimeMs((mouseBaseTime.toDouble()*(1.0/factor)).toLong())
    }

    fun getCustomFactory(): MouseMotionFactory {
        val factory = MouseMotionFactory()
        // TODO:Add Options for various flows to allow more personalization
        val flows = ArrayList<Flow>()

        // Always add random
        flows.add(Flow(Random.random()))

        flows.add(Flow(FlowTemplates.variatingFlow()))
        flows.add(Flow(FlowTemplates.slowStartupFlow()))
        flows.add(Flow(FlowTemplates.interruptedFlow()))

        flows.add(Flow(FlowTemplates.stoppingFlow()))

        manager = DefaultSpeedManager(flows)
        //TODO:Add options for custom Deviation Provider and Noise Provider
        factory.deviationProvider = SinusoidalDeviationProvider(10.0)
        factory.noiseProvider = DefaultNoiseProvider(java.lang.Double.valueOf(3.0))
        factory.nature.reactionTimeVariationMs = 50
        manager.setMouseMovementBaseTimeMs(mouseBaseTime)

        val overshootManager = factory.overshootManager as DefaultOvershootManager
        overshootManager.overshoots = Random.mid(2, 4).toInt()
        overshootManager.minOvershootMovementMs = Random.low(40, 70)

        factory.speedManager = manager

        return factory
    }
}

fun MouseMotionFactory.build(x: Int, y: Int, frame: ContainableFrame, canvas: net.runelite.api.Point): MouseMotion {
    return this.build(frame.x + x + canvas.x, frame.y + y + canvas.y)
}
fun Rectangle.getRawPoint(randomizer: (Int, Int) -> Point = defaultRandomizer): Point {
    val randPoint = randomizer.invoke(this.width, this.height)
    return Point(this.x + randPoint.x, this.y + randPoint.y)
}

fun Rectangle.getPoint(randomizer: (Int, Int) -> Point = defaultRandomizer): Point? {
    var x = -1
    var y = -1

    val randPoint = randomizer.invoke(this.width, this.height)
    x = this.x + randPoint.x
    y = this.y + randPoint.y

    if (GuiHelper.isStretchedEnabled()) {
        val wScale: Double
        val hScale: Double

        if (GuiHelper.isResized()) {
            wScale = stretchedDim.width / GuiHelper.realDim.width.toDouble()
            hScale = stretchedDim.height / GuiHelper.realDim.height.toDouble()
            val newX = (x * wScale).toInt()
            val newY = (y * hScale).toInt()
            if (newX > 0 && newX < Helper.clientUI.width) {
                if (newY > 0 && newY < Helper.frame.height) {
                    return Point(newX, newY)
                }
            }
            MouseUtil.logger.warn("Off screen point attempted. Split the step, or rotate the screen.")
            return null
        } else {
            if (x > 0 && x < Helper.frame.width) {
                if (y > 0 && y < Helper.frame.height) {
                    return Point(x, y)
                }
            }
            MouseUtil.logger.warn("Off screen point attempted. Split the step, or rotate the screen.")
            return null
        }

    } else if (!GuiHelper.isResized()) {
        val fixedWidth = Constants.GAME_FIXED_WIDTH
        var widthDif = Helper.frame.width

        widthDif -= GuiHelper.pluginPanelWidth

        widthDif -= GuiHelper.pluginPanelWidth

        widthDif -= fixedWidth
        if (x + widthDif / 2 > 0 && x + widthDif / 2 < Helper.frame.width) {
            if (y > 0 && y < Helper.frame.height) {
                return Point(x, y)
            }
        }
        MouseUtil.logger.warn("Off screen point attempted. Split the step, or rotate the screen.")
        return null
    } else {
        if (x > 0 && x < Helper.frame.width) {
            if (y > 0 && y < Helper.frame.height) {
                return Point(x, y)
            }
        }
        MouseUtil.logger.warn("Off screen point attempted. Split the step, or rotate the screen.")
        return null
    }
    return null
}