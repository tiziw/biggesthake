package net.runelite.client.plugins.biggesthake.input

import net.runelite.api.Point
import net.runelite.client.plugins.biggesthake.util.Helper
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.client
import net.runelite.client.plugins.biggesthake.util.Helper.Companion.clientUI
import net.runelite.client.ui.ClientPluginToolbar
import net.runelite.client.ui.PluginPanel
import org.apache.commons.lang3.reflect.FieldUtils
import java.awt.Component
import java.awt.Dimension
import java.awt.event.KeyEvent
import java.awt.event.MouseEvent
import java.awt.event.MouseWheelEvent

class GuiHelper {
    companion object {
        fun processMouseEvent(event: MouseEvent) {
            client.canvas.mouseListeners.forEach {
                when(event.id) {
                    MouseEvent.MOUSE_MOVED -> {
                        val meth = Component::class.java.getDeclaredMethod("processMouseMotionEvent", MouseEvent::class.java)
                        meth.isAccessible = true
                        meth.invoke(Helper.canvas, event)
                    }
                    MouseEvent.MOUSE_CLICKED -> it.mouseClicked(event)
                    MouseEvent.MOUSE_PRESSED -> it.mousePressed(event)
                    MouseEvent.MOUSE_RELEASED -> it.mouseReleased(event)
                }
            }

        }

        fun processMouseWheelEvent(event: MouseWheelEvent) {
            Helper.canvas.dispatchEvent(event)
        }

        fun processKeyEvent(event: KeyEvent) {
            Helper.canvas.dispatchEvent(event)
        }

        fun notify(message: String) {
            Helper.notifier.notify(message)
        }
        val pluginPanelWidth: Int
            get() {
                if(Helper.pluginPanel == null) {
                    val tmp = FieldUtils.readField(clientUI, "pluginPanel", true)
                    Helper.pluginPanel ?:return 0

                    Helper.pluginPanel = tmp as PluginPanel
                }
                return Helper.pluginPanel!!.width
            }

        val pluginToolbarWidth: Int
            get() {
                if(Helper.pluginToolbar == null) {
                    val tmp = FieldUtils.readField(clientUI, "pluginToolbar", true) as ClientPluginToolbar
                    Helper.pluginToolbar ?:return 0

                    Helper.pluginToolbar = tmp as ClientPluginToolbar
                }
                Helper.pluginToolbar ?:return 0
                return Helper.pluginToolbar!!.width
            }
        val stretchedDim: Dimension
            get() {
                return client.stretchedDimensions
            }

        val realDim: Dimension
            get() {
                return client.realDimensions
            }

        val scale: Int
            get() {
                return client.scale
            }

        val mouseX: Int
            get() {
                return client.mouseCanvasPosition.x
            }
        val mouseY: Int
            get() {
                return client.mouseCanvasPosition.y
            }

        fun getCanvasOffset(): Point {
            return clientUI.canvasOffset
        }

        fun isResized(): Boolean {
            return client.isResized
        }

        fun isStretchedEnabled(): Boolean {
            return client.isStretchedEnabled
        }
    }
}