package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.utils

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.getNearestNPCs
import net.runelite.client.plugins.biggesthake.api.rs.hasAction
import net.runelite.client.plugins.biggesthake.api.rs.interact
import net.runelite.client.plugins.biggesthake.api.rs.isWidgetOpen
import net.runelite.client.plugins.biggesthake.enums.WidgetGroup


object CharterShip {
    enum class Destination // $FF: synthetic method
    (val action: String, val index: Int, val position: WorldPoint) {
        MOS_LE_HARMLESS("Mos Le'Harmless", 26, WorldPoint(3671, 2931, 0)),
        CORSAIR_COVE("Corsair Cove", 32, WorldPoint(2587, 2851, 0)),
        CATHERBY("Catherby", 8, WorldPoint(2792, 3414, 0)),
        PORT_SARIM("Port Sarim", 23, WorldPoint(3038, 3192, 0)),
        PORT_KHAZARD("Port Khazard", 20, WorldPoint(2674, 3144, 0)),
        BRIMHAVEN("Brimhaven", 17, WorldPoint(2760, 3239, 0)),
        PORT_PHASMATYS("Port Phasmatys", 5, WorldPoint(3702, 3503, 0)),
        PORT_TYRAS("Port Tyras", 2, WorldPoint(2142, 3122, 0)),
        MUSA_POINT("Musa Point", 14, WorldPoint(2954, 3155, 0)),
        SHIPYARD("Shipyard", 11, WorldPoint(3001, 3032, 0));
    }

    fun charter(destination: Destination) :Boolean {
        return false
    }

    fun isInterfaceOpen(): Boolean {
        return isWidgetOpen(WidgetGroup.CHARTER_SHIP.groupId, 0)
    }

    fun openCharterMenu(): Boolean {
        val npc = getNearestNPCs().firstOrNull { it.hasAction { action -> action == "Charter" }}?:return false
        return npc.interact(action = "Charter")
    }
}