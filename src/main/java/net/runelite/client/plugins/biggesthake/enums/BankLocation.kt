package net.runelite.client.plugins.biggesthake.enums

import net.runelite.api.coords.WorldPoint
import net.runelite.client.plugins.biggesthake.api.rs.WorldPoint

enum class Type {
    BANK_CHEST,
    BANK_BOOTH,
    NPC,
    DEPOSIT_BOX
}

enum class BankLocation(val objectName: String, val action: String, val tile: WorldPoint, val type: Type, val condition: () -> Boolean) {
    PORT_KHAZARD("Bank chest", "Use", WorldPoint(2661, 3163, 0), Type.BANK_CHEST),
    VARROCK_WEST(WorldPoint(3185, 3436)),
    SHANTAY_PASS("Bank chest", "Use", WorldPoint(3309, 3120), Type.BANK_CHEST),
    SHAYZIEN_HOUSE(WorldPoint(1505, 3615)),
    FARMING_GUILD(WorldPoint(1253, 3741)
            /*,BooleanSupplier { Skills.getCurrentLevel(Skill.FARMING) >= 45 && Functions.mapOrElse(Worlds::getLocal, RSWorld::isMembers) }*/),
    ARCEUUS_HOUSE(WorldPoint(1629, 3745)),
    CASTLE_WARS("Bank chest", "Use", WorldPoint(2443, 3083), Type.BANK_CHEST),
    FALADOR_WEST(WorldPoint(2947, 3368)),
    MINING_GUILD(WorldPoint(3013, 9718) /*,BooleanSupplier { Skills.getCurrentLevel(Skill.MINING) >= 60 && Functions.mapOrElse(Worlds::getLocal, RSWorld::isMembers) }*/),
    ZEAH_SHORE("Bank chest", "Use", WorldPoint(1720, 3465), Type.BANK_CHEST),
    WINTERTODT("Bank chest", "Bank", WorldPoint(1639, 3943), Type.BANK_CHEST),
    VARROCK_EAST(WorldPoint(3253, 3420)),
    DRAYNOR(WorldPoint(3092, 3245)),
    ARDOUGNE_SOUTH_DB("Bank deposit box", "Deposit", WorldPoint(2654, 3281), Type.DEPOSIT_BOX
//            ,BooleanSupplier { Functions.mapOrElse(Worlds::getLocal, RSWorld::isMembers) }
    ),
    YANILLE(WorldPoint(2613, 3094)),
    CATHERBY_DB("Bank deposit box", "Deposit", WorldPoint(2729, 3492), Type.DEPOSIT_BOX
//            , BooleanSupplier { Functions.mapOrElse(Worlds::getLocal, RSWorld::isMembers) }
    ),
    EDGEVILLE(WorldPoint(3094, 3491)),
    WOODCUTTING_GUILD(WorldPoint(1588, 3477)
//            , BooleanSupplier { Skills.getCurrentLevel(Skill.WOODCUTTING) >= 60 && Functions.mapOrElse(Worlds::getLocal, RSWorld::isMembers) }
    ),
    LLETYA(WorldPoint(2353, 3165)),
    CAMELOT_PVP("Bank chest", "Use", WorldPoint(2756, 3479, 0), Type.BANK_CHEST, {false}
//            BooleanSupplier { Functions.mapOrElse(Worlds::getLocal, RSWorld::isPVP) }
    ),
    EDGEVILLE_DB("Bank deposit box", "Deposit", WorldPoint(3098, 3498), Type.DEPOSIT_BOX),
    MOTHERLODE_MINE("Bank chest", "Use", WorldPoint(3761, 5666), Type.BANK_CHEST),
    CLAN_WARS("Bank chest", "Use", WorldPoint(3369, 3170, 0), Type.BANK_CHEST),
    MISCELLANIA("Banker", "Bank", WorldPoint(2618, 3895), Type.NPC),
    AL_KHARID(WorldPoint(3269, 3167)),
    DRAYNOR_VILLAGE_DB("Bank deposit box", "Deposit", WorldPoint(3094, 3241), Type.DEPOSIT_BOX),
    CANIFIS(WorldPoint(3512, 3480));

    // $FF: synthetic method
    constructor(position: WorldPoint, condition: () -> Boolean) : this("Bank booth", "Bank", position, Type.BANK_BOOTH, condition) {}

    // $FF: synthetic method
    constructor(name: String, action: String, tile: WorldPoint, type: Type) : this(name, action, tile, type, { true })

    // $FF: synthetic method
    constructor(position: WorldPoint) : this(position, { true })
}