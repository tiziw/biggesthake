package net.runelite.client.plugins.biggesthake.api.script

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import kotlin.reflect.KClass

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
annotation class Event(val onStart: Boolean, val type: KClass<*>)
