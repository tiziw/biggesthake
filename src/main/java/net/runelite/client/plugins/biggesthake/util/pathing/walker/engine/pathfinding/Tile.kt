package net.runelite.client.plugins.biggesthake.util.pathing.walker.engine.pathfinding

import net.runelite.api.coords.WorldPoint

class Tile internal constructor(var rsTile: net.runelite.api.Tile, val x: Int, val y: Int, val z: Int) {
    //constructor() : this()

    var isBlocked: Boolean = false
    var isBlockedN: Boolean = false
    var isBlockedE: Boolean = false
    var isBlockedS: Boolean = false
    var isBlockedW: Boolean = false

    fun setFlag(flag: Int) {
        isBlocked = TileFlag.BLOCKED.valid(flag)
        isBlockedN = TileFlag.BLOCKED_NORTH.valid(flag)
        isBlockedE = TileFlag.BLOCKED_EAST.valid(flag)
        isBlockedS = TileFlag.BLOCKED_SOUTH.valid(flag)
        isBlockedW = TileFlag.BLOCKED_WEST.valid(flag)
    }

    fun toWorldPoint(): WorldPoint {
        return WorldPoint(x, y, z)
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is Tile) return false
        val tile = o
        if (x != tile.x) return false
        if (y != tile.y) return false
        if (z != tile.z) return false
        if (isBlocked != tile.isBlocked) return false
        if (isBlockedN != tile.isBlockedN) return false
        if (isBlockedE != tile.isBlockedE) return false
        return if (isBlockedS != tile.isBlockedS) false else isBlockedW == tile.isBlockedW
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        result = 31 * result + z
        result = 31 * result + if (isBlocked) 1 else 0
        result = 31 * result + if (isBlockedN) 1 else 0
        result = 31 * result + if (isBlockedE) 1 else 0
        result = 31 * result + if (isBlockedS) 1 else 0
        result = 31 * result + if (isBlockedW) 1 else 0
        return result
    }

    override fun toString(): String {
        return String.format("(x=%d, y=%d, z=%d)", x, y, z)
    }

}