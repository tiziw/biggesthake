package net.runelite.client.plugins.biggesthake

import net.runelite.client.config.Config
import net.runelite.client.config.ConfigGroup
import net.runelite.client.config.ConfigItem

@ConfigGroup("general")
interface BHConfiguration : Config {
    @ConfigItem(keyName = "Current script", name = "Script", description = "")
    fun script(): LocalScripts {
        return LocalScripts.TEST_1
    }

    @ConfigItem(keyName = "Enter a name", name = "name", description = "")
    fun text(): String {
        return "Banker"
    }

    @ConfigItem(keyName = "enabled", name = "Enable overlay", description = "Configures whether the overlay is enabled")
    fun enabled(): Boolean {
        return false
    }
}
