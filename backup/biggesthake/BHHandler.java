package net.runelite.client.plugins.biggesthake;

import lombok.Getter;
import net.runelite.api.Client;
import net.runelite.client.plugins.biggesthake.BHConfiguration;
import net.runelite.client.plugins.biggesthake.BHPlugin;
import net.runelite.client.plugins.biggesthake.api.BaseScript;
import net.runelite.client.plugins.biggesthake.input.MouseDriver;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class BHHandler {
    @Getter
    private Mouse mouse;

    ThreadGroup threadGroup = new ThreadGroup("Scripts");
    private List<Thread> runningThreads = new ArrayList<>();

    public BHHandler(BHPlugin plugin, BHConfiguration config) {
        try {
            this.mouse = new Mouse(plugin.clientUI);
        } catch (AWTException e) {
            e.printStackTrace();
        }

        try {
            BaseScript script = (BaseScript) plugin.config.script().getScriptClass().getConstructor(Client.class).newInstance(plugin.client);
            Thread thread = new Thread(threadGroup, () -> script.start());
            runningThreads.add(thread);
            thread.start();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        threadGroup.destroy();
    }
}
