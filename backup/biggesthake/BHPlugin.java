package net.runelite.client.plugins.biggesthake;

import com.google.inject.Binder;
import com.google.inject.Provides;
import javax.annotation.Nullable;
import javax.inject.Inject;

import net.runelite.api.Client;
import net.runelite.client.config.ConfigManager;
import net.runelite.client.plugins.Plugin;
import net.runelite.client.plugins.PluginDescriptor;
import net.runelite.client.ui.ClientUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PluginDescriptor(
		name = "Big Hake Plugin",
		description = "Show helpful information",
		tags = {"overlay"}
)
public class BHPlugin extends Plugin
{
	private static final Logger logger = LoggerFactory.getLogger(BHPlugin.class);
	private static BHPlugin plugin;

	@Inject
	@Nullable
	public Client client;

	@Inject
	public
	ClientUI clientUI;

	@Inject
	public
	BHConfiguration config;

	@Inject
	BHOverlay overlay;

	BHHandler handler;

	@Override
	protected void startUp() throws Exception
	{
		this.plugin = this;
		handler = new BHHandler(this, config);

		logger.info("Example plugin started!");
	}

	@Override
	protected void shutDown() throws Exception
	{
		logger.info("Example plugin stopped!");
		handler.shutdown();
	}

	public static Client getClient() {
		return plugin.client;
	}

	@Override
	public void configure(Binder binder)
	{
		binder.bind(BHOverlay.class);
	}

	@Provides
	BHConfiguration provideConfig(ConfigManager configManager)
	{
		return configManager.getConfig(BHConfiguration.class);
	}

}

