package net.runelite.client.plugins.biggesthake.api

import net.runelite.api.Client
import net.runelite.api.NPC
import net.runelite.client.plugins.biggesthake.BHPlugin
import net.runelite.client.plugins.biggesthake.LocalScripts
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.logging.Level


abstract class BaseScript(val client: Client) {

    fun start() {
        getLogger().info("Starting script")
        run();
    }

    fun getLogger() : Logger {
        return LoggerFactory.getLogger(getScript()?.getScriptClass())
    }

    abstract fun getScript(): LocalScripts?

    abstract fun run();

    fun getNearestNPCs(): List<NPC> {
        return client.npcs.sortedBy { it.worldLocation.distanceTo(client.localPlayer?.worldLocation) };
    }

    fun getNearestNPCs(name: String): NPC {
        return getNearestNPCs().first { it.name?.toLowerCase().equals(name.toLowerCase()) }
    }
}
