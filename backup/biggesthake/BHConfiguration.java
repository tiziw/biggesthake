package net.runelite.client.plugins.biggesthake;

import net.runelite.client.config.Config;
import net.runelite.client.config.ConfigGroup;
import net.runelite.client.config.ConfigItem;

@ConfigGroup("general"
)
public interface BHConfiguration extends Config
{
	@ConfigItem(
			keyName = "Current script",
			name = "Script",
			description = ""
	)
	default LocalScripts script()
	{
		return LocalScripts.TEST_1;
	}

	@ConfigItem(
			keyName = "Enter a name",
			name = "name",
			description = ""
	)
	default String text() {return "Banker";}

	@ConfigItem(
		keyName = "enabled",
		name = "Enable overlay",
		description = "Configures whether the overlay is enabled"
	)
	default boolean enabled()
	{
		return false;
	}
}
