package net.runelite.client.plugins.biggesthake

import net.runelite.client.plugins.biggesthake.scripts.TestScript
import kotlin.reflect.KClass

enum class LocalScripts(name: String, val scriptKClass: KClass<*>) {
    TEST_1("Test1", TestScript::class);

    fun getScriptClass(): Class<out Any> {
        return scriptKClass?.java
    }

    override fun toString(): String {
        return name
    }
}
