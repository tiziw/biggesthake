package net.runelite.client.plugins.biggesthake

import net.runelite.api.NPC
import net.runelite.client.plugins.biggesthake.mouse.Mouse

fun NPC.click() {
    Mouse.click(this.convexHull.bounds);
}