package net.runelite.client.plugins.biggesthake.scripts

import net.runelite.api.Client
import net.runelite.api.GameState
import net.runelite.client.plugins.biggesthake.LocalScripts
import net.runelite.client.plugins.biggesthake.Scripts
import net.runelite.client.plugins.biggesthake.api.BaseScript
import net.runelite.client.plugins.biggesthake.click

class TestScript(client: Client) : BaseScript(client) {
    override fun getScript(): LocalScripts {
        return LocalScripts.TEST_1
    }

    override fun run() {
        while(client.gameState != GameState.LOGGED_IN) {
            Thread.sleep(100);
        }
        Thread.sleep(10234);
        getLogger().info("Woke up, going to click banker")
        getNearestNPCs("Banker").click()
    }
}
