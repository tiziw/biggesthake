package net.runelite.client.plugins.biggesthake.input;

import com.github.joonasvali.naturalmouse.api.MouseMotionFactory;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.logging.Logger;

import net.runelite.api.Constants;
import net.runelite.client.plugins.biggesthake.BHPlugin;
import net.runelite.client.ui.ClientPluginToolbar;
import net.runelite.client.ui.ClientUI;
import net.runelite.client.ui.ContainableFrame;
import net.runelite.client.ui.PluginPanel;
import org.apache.commons.lang3.reflect.FieldUtils;

import javax.inject.Singleton;

@Singleton
public class Mouse extends Robot
{
    private static Mouse instance;
    public ThreadGroup flexoThreads = new ThreadGroup("bighake");

    public static boolean isActive;
    public static final int fixedWidth = Constants.GAME_FIXED_WIDTH;
    public static final int fixedHeight = Constants.GAME_FIXED_HEIGHT;
    public static int minDelay = 45;
    public static MouseMotionFactory currentMouseMotionFactory;
    public boolean pausedIndefinitely = false;
    private Robot peer;

    protected static ClientUI clientUI;
    protected static ContainableFrame frame;
    protected static PluginPanel pluginPanel;
    protected static ClientPluginToolbar pluginToolbar;

    public Mouse(ClientUI clientUI) throws AWTException
    {
        this.clientUI = clientUI;
        this.instance = this;
        try {
            this.frame = (ContainableFrame) FieldUtils.readField(clientUI, "frame", true);
            this.pluginPanel = (PluginPanel) FieldUtils.readField(clientUI, "pluginPanel", true);
            this.pluginToolbar = (ClientPluginToolbar) FieldUtils.readField(clientUI, "pluginToolbar", true);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (GraphicsEnvironment.isHeadless())
        {
            throw new AWTException("headless environment");
        }

        init(GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getDefaultScreenDevice());
    }

    private void init(GraphicsDevice screen)
    {
        try
        {
            peer = new Robot();
        }
        catch (Exception e)
        {
            BHPlugin.getClient().getLogger().error("Mouse not supported on this system configuration.");
        }
    }

    private transient Object anchor = new Object();

    private void pauseMS(int delayMS)
    {
        long initialMS = System.currentTimeMillis();
        while (System.currentTimeMillis() < initialMS + delayMS)
        {
            try
            {
                Thread.sleep(10);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        isActive = false;
    }

    public static void click(Rectangle r) {
        r = MouseUtil.getClickArea(r);
        Point p = MouseUtil.getClickPoint(r);
        instance.mouseMove(p);
        instance.mousePressAndRelease(1);
    }

    @Override
    public synchronized void mouseMove(int x, int y)
    {
        try
        {
            currentMouseMotionFactory.build(frame.getX() + x + determineHorizontalOffset(),
                    frame.getY() + y + determineVerticalOffset()).move();
            this.delay(getMinDelay());
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public synchronized void mouseMove(Point p)
    {
        mouseMove((int) p.getX(), (int) p.getY());
        try
        {
            Thread.sleep(150);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void mousePress(int buttonID)
    {
        if (buttonID < 1 || buttonID > 5)
        {
            Logger.getAnonymousLogger().warning("Invalid mouse button ID. please use 1-5.");
            return;
        }
        peer.mousePress(InputEvent.getMaskForButton(buttonID));
        this.delay(getMinDelay());
    }

    public synchronized void mousePressAndRelease(int buttonID)
    {
        if (buttonID < 1 || buttonID > 5)
        {
            Logger.getAnonymousLogger().warning("Invalid mouse button ID. please use 1-5.");
            return;
        }
        peer.mousePress(InputEvent.getMaskForButton(buttonID));
        this.delay(getMinDelay());
        peer.mouseRelease(InputEvent.getMaskForButton(buttonID));
        this.delay(getMinDelay());
    }

    //TODO: Symbols are nut supported at this time
    public synchronized void typeMessage(String message)
    {

        Random r = new Random();
        char[] charArray = message.toCharArray();
        for (char c : charArray)
        {
            keyPress(KeyEvent.getExtendedKeyCodeForChar(c));
            this.delay(93 + r.nextInt(getMinDelay()));
        }
        keyPress(KeyEvent.VK_ENTER);
        this.delay(93 + r.nextInt(getMinDelay()));
        //clientUI.allowInput = true;
    }


    @Override
    public synchronized void mouseRelease(int buttonID)
    {
        if (buttonID < 1 || buttonID > 5)
        {
            Logger.getAnonymousLogger().warning("Invalid mouse button ID. please use 1-5.");
            return;
        }
        peer.mouseRelease(InputEvent.getMaskForButton(buttonID));
        this.delay(getMinDelay());
    }

    private int getMinDelay()
    {
        Random random = new Random();
        int random1 = random.nextInt(minDelay);
        if (random1 < minDelay / 2)
        {
            random1 = random.nextInt(minDelay / 2) + minDelay / 2 + random.nextInt(minDelay / 2);
        }
        return random1;
    }

    private int getWheelDelay()
    {
        Random random = new Random();
        int random1 = random.nextInt(minDelay);
        if (random1 < minDelay / 2)
        {
            random1 = random.nextInt(minDelay / 2) + minDelay / 2 + random.nextInt(minDelay / 2);
        }
        return random1;
    }

    /**
     * Rotates the scroll wheel on wheel-equipped mice.
     *
     * @param wheelAmt number of "notches" to move the mouse wheel
     *                 Negative values indicate movement up/away from the user,
     *                 positive values indicate movement down/towards the user.
     * @since 1.4
     */
    @Override
    public synchronized void mouseWheel(int wheelAmt)
    {
        for (int i : new int[wheelAmt])
        {
            peer.mouseWheel(wheelAmt);
            this.delay(getWheelDelay());
        }
    }

    /**
     * Presses a given key.  The key should be released using the
     * <code>keyRelease</code> method.
     * <p>
     * Key codes that have more than one physical key associated with them
     * (e.g. <code>KeyEvent.VK_SHIFT</code> could mean either the
     * left or right shift key) will map to the left key.
     *
     * @param keycode Key to press (e.g. <code>KeyEvent.VK_A</code>)
     * @throws IllegalArgumentException if <code>keycode</code> is not
     *                                  a valid key
     * @see #keyRelease(int)
     * @see java.awt.event.KeyEvent
     */
    @Override
    public synchronized void keyPress(int keycode)
    {
        peer.keyPress(keycode);
        this.delay(getMinDelay());
    }

    @Override
    public synchronized void keyRelease(int keycode)
    {
        peer.keyRelease(keycode);
        this.delay(getMinDelay());
    }

    public synchronized void holdKey(int keycode, int timeMS)
    {
        new Thread(() ->
        {
            peer.keyPress(keycode);
            long startTime = System.currentTimeMillis();
            while ((startTime + timeMS) > System.currentTimeMillis())
            {
            }
            peer.keyRelease(keycode);
            this.delay(getMinDelay());
        }).start();
    }

    public synchronized void holdKeyIndefinitely(int keycode)
    {
        Thread holdKeyThread = new Thread(() ->
        {
            pausedIndefinitely = true;
            peer.keyPress(keycode);
            while (pausedIndefinitely)
            {
                try
                {
                    Thread.sleep(10);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            peer.keyRelease(keycode);
            this.delay(getMinDelay());
        });
        holdKeyThread.start();

    }

    public Color getPixelColor(int x, int y)
    {
        return peer.getPixelColor(x, y);
    }

    @Override
    public void delay(int ms)
    {
        pauseMS(ms);
    }

    public int determineHorizontalOffset()
    {
        return clientUI.getCanvasOffset().getX();
    }

    public int determineVerticalOffset()
    {
        return clientUI.getCanvasOffset().getY();
    }

}