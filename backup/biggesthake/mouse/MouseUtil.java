

package net.runelite.client.plugins.biggesthake.input;

import com.github.joonasvali.naturalmouse.api.MouseMotionFactory;
import com.github.joonasvali.naturalmouse.support.*;
import com.github.joonasvali.naturalmouse.util.FlowTemplates;
import net.runelite.api.Constants;
import net.runelite.client.plugins.biggesthake.BHPlugin;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import static net.runelite.client.plugins.biggesthake.input.MouseDriver.*;

public class MouseUtil {

    private void updateMouseMotionFactory() {
        MouseMotionFactory factory = new MouseMotionFactory();
        // TODO:Add Options for various flows to allow more personalization
        List<Flow> flows = new ArrayList<>();

        // Always add random
        flows.add(new Flow(FlowTemplates.random()));

        flows.add(new Flow(FlowTemplates.variatingFlow()));
        flows.add(new Flow(FlowTemplates.slowStartupFlow()));
        flows.add(new Flow(FlowTemplates.interruptedFlow()));

        flows.add(new Flow(FlowTemplates.stoppingFlow()));

        DefaultSpeedManager manager = new DefaultSpeedManager(flows);
        //TODO:Add options for custom Deviation Provider and Noise Provider
        factory.setDeviationProvider(new SinusoidalDeviationProvider(18));
        factory.setNoiseProvider(new DefaultNoiseProvider(Double.valueOf(2.0D)));
        factory.getNature().setReactionTimeVariationMs(80);
        manager.setMouseMovementBaseTimeMs(49);

        DefaultOvershootManager overshootManager = (DefaultOvershootManager) factory.getOvershootManager();
        overshootManager.setOvershoots(4);

        factory.setSpeedManager(manager);
        Mouse.currentMouseMotionFactory = factory;
    }

    /*
    Should pass unstretched coords, handles all conversions here.
    */
    public static Point getClickPoint(Rectangle rect) {
        if (rect != null) {
            Random r = new Random();
            int x = -1;
            int y = -1;
            x = rect.x + r.nextInt(rect.width);
            y = rect.y + r.nextInt(rect.height);

            if (BHPlugin.getClient().isStretchedEnabled()) {
                double wScale;
                double hScale;

                if (BHPlugin.getClient().isResized()) {
                    wScale = (BHPlugin.getClient().getStretchedDimensions().width / (double) BHPlugin.getClient().getRealDimensions().width);
                    hScale = (BHPlugin.getClient().getStretchedDimensions().height / (double) BHPlugin.getClient().getRealDimensions().height);
                    int newX = (int) (x * wScale);
                    int newY = (int) (y * hScale);
                    if (newX > 0 && newX < clientUI.getWidth()) {
                        if (newY > 0 && newY < frame.getHeight()) {
                            return new Point(newX, newY);
                        }
                    }
                    Logger.getAnonymousLogger().warning("[RuneLit]Flexo - Off screen point attempted. Split the step, or rotate the screen.");
                    return null;
                } else {
                    if (x > 0 && x < frame.getWidth()) {
                        if (y > 0 && y < frame.getHeight()) {
                            return new Point(x, y);
                        }
                    }
                    Logger.getAnonymousLogger().warning("[RuneLit]Flexo - Off screen point attempted. Split the step, or rotate the screen.");
                    return null;
                }

            } else if (!BHPlugin.getClient().isResized()) {
                final int fixedWidth = Constants.GAME_FIXED_WIDTH;
                int widthDif = frame.getWidth();

                if (pluginToolbar.isVisible()) {
                    widthDif -= pluginToolbar.getWidth();
                }
                if (pluginPanel != null) {
                    widthDif -= pluginPanel.getWidth();
                }

                widthDif -= fixedWidth;
                if (x + (widthDif / 2) > 0 && x + (widthDif / 2) < frame.getWidth()) {
                    if (y > 0 && y < frame.getHeight()) {
                        return new Point(x, y);
                    }
                }
                Logger.getAnonymousLogger().warning("[RuneLit]Flexo - Off screen point attempted. Split the step, or rotate the screen.");
                return null;
            } else {
                if (x > 0 && x < frame.getWidth()) {
                    if (y > 0 && y < frame.getHeight()) {
                        return new Point(x, y);
                    }
                }
                Logger.getAnonymousLogger().warning("[RuneLit]Flexo - Off screen point attempted. Split the step, or rotate the screen.");
                return null;
            }
        }
        return null;
    }

    public static Rectangle getClickArea(Rectangle rect) {
        if (BHPlugin.getClient().isStretchedEnabled()) {
            double wScale;
            double hScale;

            if (BHPlugin.getClient().isResized()) {
                wScale = (BHPlugin.getClient().getStretchedDimensions().width / (double) BHPlugin.getClient().getRealDimensions().width);
                hScale = (BHPlugin.getClient().getStretchedDimensions().height / (double) BHPlugin.getClient().getRealDimensions().height);
            } else {
                wScale = (BHPlugin.getClient().getStretchedDimensions().width) / (double) Mouse.fixedWidth;
                hScale = (BHPlugin.getClient().getStretchedDimensions().height) / (double) Mouse.fixedHeight;
            }

            int xPadding = (int) rect.getWidth() / 5;
            int yPadding = (int) rect.getHeight() / 5;
            Random r = new Random();
            Rectangle clickRect = new Rectangle();
            clickRect.width = rect.width - xPadding * 2;
            clickRect.height = rect.height - yPadding * 2;
            clickRect.x = rect.x + xPadding;
            clickRect.y = rect.y + yPadding;
            if (clickRect.width > 0 && clickRect.height > 0) {
                int x = clickRect.x + r.nextInt(clickRect.width);
                int y = clickRect.y + r.nextInt(clickRect.height);
                double tScale = 1 + (BHPlugin.getClient().getScale() / 100);

                if (BHPlugin.getClient().isResized()) {
                    return new Rectangle((int) (clickRect.x * wScale), (int) (clickRect.y * wScale), (int) (clickRect.width * wScale), (int) (clickRect.getHeight() * hScale));
                } else {
                    return new Rectangle(clickRect.x, clickRect.y, clickRect.width, (int) (clickRect.getHeight()));
                }
            }

        }
        //Fixed, not stretched
        else if (!BHPlugin.getClient().isResized()) {
            int fixedWidth = 765;
            int widthDif = frame.getWidth();

            if (pluginToolbar.isVisible()) {
                widthDif -= pluginToolbar.getWidth();
            }
            if (pluginPanel != null) {
                widthDif -= pluginPanel.getWidth();
            }

            widthDif -= fixedWidth;
            int xPadding = (int) rect.getWidth() / 5;
            int yPadding = (int) rect.getHeight() / 5;
            Random r = new Random();
            Rectangle clickRect = new Rectangle();
            clickRect.width = rect.width - xPadding;
            clickRect.height = rect.height - yPadding;
            clickRect.x = rect.x + xPadding;
            clickRect.y = rect.y + yPadding;
            if (clickRect.height > 0 && clickRect.width > 0) {
                int x = clickRect.x + r.nextInt(clickRect.width);
                int y = clickRect.y + r.nextInt(clickRect.height);
                return new Rectangle(clickRect.x, clickRect.y, clickRect.width, (int) (clickRect.getHeight()));
            }
        }
        //Resizable, not stretched
        else {
            int xPadding = (int) rect.getWidth() / 5;
            int yPadding = (int) rect.getHeight() / 5;
            Random r = new Random();
            Rectangle clickRect = new Rectangle();
            clickRect.width = rect.width - xPadding * 2;
            clickRect.height = rect.height - yPadding * 2;
            clickRect.x = rect.x + xPadding;
            clickRect.y = rect.y + yPadding;
            if (clickRect.height > 0 && clickRect.width > 0) {
                int x = clickRect.x + r.nextInt(clickRect.width);
                int y = clickRect.y + r.nextInt(clickRect.height);
                return new Rectangle(clickRect.x, clickRect.y, clickRect.width, (int) (clickRect.getHeight()));
            }
        }

        return null;
    }
}